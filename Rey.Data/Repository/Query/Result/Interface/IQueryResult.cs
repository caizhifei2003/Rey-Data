﻿using System.Collections.Generic;

namespace Rey.Data {
    public interface IQueryResult<TModel> {
        IEnumerable<TModel> Items { get; }
        long Total { get; }
        long Skip { get; }
    }
}
