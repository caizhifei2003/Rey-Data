﻿using System.Collections.Generic;
using System.Linq;

namespace Rey.Data {
    public class QueryResult<TModel> : IQueryResult<TModel> {
        public IEnumerable<TModel> Items { get; set; }
        public long Total { get; set; }
        public long Skip { get; set; }

        public QueryResult(IEnumerable<TModel> items, long total, long skip) {
            this.Items = items;
            this.Total = total;
            this.Skip = skip;
        }

        public QueryResult(IEnumerable<TModel> items, long total)
            : this(items, total, 0) {
        }

        public QueryResult(IEnumerable<TModel> items)
            : this(items, items.Count()) {
        }

        public QueryResult()
            : this(new List<TModel>()) {
        }
    }
}
