﻿namespace Rey.Data {
    public interface IQueryPage {
        long? Take { get; }
        long? Skip { get; }
    }
}
