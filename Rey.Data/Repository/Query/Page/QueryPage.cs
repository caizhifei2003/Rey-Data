﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public class QueryPage : IQueryPage {
        public long? Take { get; set; }
        public long? Skip { get; set; }

        public QueryPage(long take, long skip) {
            this.Take = take;
            this.Skip = skip;
        }

        public QueryPage(long take) {
            this.Take = take;
        }

        public QueryPage() {

        }

        public static QueryPage Parse(IEnumerable<KeyValuePair<string, StringValues>> query) {
            var page = new QueryPage();
            foreach (var pair in query) {
                var key = pair.Key;
                var values = pair.Value;
                var match = Regex.Match(key, $"page\\.(?<name>\\w+)", RegexOptions.IgnoreCase);
                if (match.Success) {
                    var name = match.Groups["name"].Value;

                    if (name.Equals("take", StringComparison.CurrentCultureIgnoreCase)
                        && long.TryParse(values.First(), out var take)) {
                        page.Take = take;
                        continue;
                    }

                    if (name.Equals("skip", StringComparison.CurrentCultureIgnoreCase)
                        && long.TryParse(values.First(), out var skip)) {
                        page.Skip = skip;
                        continue;
                    }
                }
            }
            return page;
        }
    }
}
