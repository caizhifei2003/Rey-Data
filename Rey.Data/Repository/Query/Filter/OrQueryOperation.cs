﻿namespace Rey.Data {
    public class OrQueryOperation : QueryOperation {
        public override string Name => "Or";
    }
}
