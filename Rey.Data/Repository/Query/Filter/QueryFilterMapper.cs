﻿using System;
using System.Linq;

namespace Rey.Data {
    public class QueryFilterMapper<TModel> : IQueryFilterMapper<TModel> {
        private IQueryFilter Filter { get; }
        private IFilterBuilder<TModel> Builder { get; }

        public QueryFilterMapper(IQueryFilter filter, IFilterBuilder<TModel> builder) {
            this.Filter = filter;
            this.Builder = builder;
        }

        public IQueryFilterMapper<TModel> Map(string name, Action<IQueryFilterItem, IFilterBuilder<TModel>> build) {
            var item = this.Filter.Items.Where(x => x.Name.Equals(name)).FirstOrDefault();
            if (item == null)
                return this;

            build(item, this.Builder);
            return this;
        }
    }
}
