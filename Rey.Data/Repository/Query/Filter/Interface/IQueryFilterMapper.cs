﻿using System;

namespace Rey.Data {
    public interface IQueryFilterMapper<TModel> {
        IQueryFilterMapper<TModel> Map(string name, Action<IQueryFilterItem, IFilterBuilder<TModel>> build);
    }
}
