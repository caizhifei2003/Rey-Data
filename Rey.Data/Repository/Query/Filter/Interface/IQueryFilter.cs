﻿using System;
using System.Collections.Generic;

namespace Rey.Data {
    public interface IQueryFilter {
        IEnumerable<IQueryFilterItem> Items { get; }
        IQueryOperation Operation { get; }
        IFilter<TModel> ToFilter<TModel>(Action<IQueryFilterMapper<TModel>> map);
    }
}
