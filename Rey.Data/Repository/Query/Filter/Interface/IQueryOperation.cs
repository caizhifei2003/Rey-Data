﻿using System;

namespace Rey.Data {
    public interface IQueryOperation : IEquatable<IQueryOperation> {
        string Name { get; }
    }
}
