﻿using Microsoft.Extensions.Primitives;

namespace Rey.Data {
    public interface IQueryFilterItem {
        string Name { get; }
        StringValues Values { get; }
        IQueryOperation Operation { get; }

        string First(string defaultValue = null);
        T FirstAs<T>(T defaultValue = default(T));

        string Last(string defaultValue = null);
        T LastAs<T>(T defaultValue = default(T));
    }
}
