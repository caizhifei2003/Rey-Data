﻿using System;

namespace Rey.Data {
    public abstract class QueryOperation : IQueryOperation {
        public abstract string Name { get; }

        public bool Equals(IQueryOperation other) {
            return this.Name.Equals(other.Name);
        }

        public override string ToString() {
            return this.Name;
        }

        public static QueryOperation And { get; } = new AndQueryOperation();
        public static QueryOperation Or { get; } = new OrQueryOperation();

        public static QueryOperation Parse(string name) {
            if (name.Equals("and", StringComparison.CurrentCultureIgnoreCase))
                return And;

            if (name.Equals("or", StringComparison.CurrentCultureIgnoreCase))
                return Or;

            throw new InvalidOperationException($"invalid operation name {name}");
        }
    }
}
