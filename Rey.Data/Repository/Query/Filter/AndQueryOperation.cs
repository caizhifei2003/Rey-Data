﻿namespace Rey.Data {
    public class AndQueryOperation : QueryOperation {
        public override string Name => "And";
    }
}
