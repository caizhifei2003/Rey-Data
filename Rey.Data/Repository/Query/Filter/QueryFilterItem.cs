﻿using Microsoft.Extensions.Primitives;
using System;
using System.Linq;

namespace Rey.Data {
    public class QueryFilterItem : IQueryFilterItem {
        public string Name { get; set; }
        public StringValues Values { get; set; }
        public IQueryOperation Operation { get; set; }

        public QueryFilterItem(string name, StringValues values, IQueryOperation operation) {
            this.Name = name;
            this.Values = values;
            this.Operation = operation;
        }

        public QueryFilterItem(string name, StringValues values)
            : this(name, values, null) {
        }

        public QueryFilterItem(string name)
            : this(name, StringValues.Empty) {
        }

        public QueryFilterItem()
            : this(null) {
        }

        public string First(string defaultValue = null) {
            if (this.Values.Count == 0)
                return defaultValue;

            return this.Values.First();
        }

        public T FirstAs<T>(T defaultValue = default(T)) {
            if (this.Values.Count == 0)
                return defaultValue;

            return (T)Convert.ChangeType(this.Values.First(), typeof(T));
        }

        public T LastAs<T>(T defaultValue = default(T)) {
            if (this.Values.Count == 0)
                return defaultValue;

            return (T)Convert.ChangeType(this.Values.Last(), typeof(T));
        }

        public string Last(string defaultValue = null) {
            if (this.Values.Count == 0)
                return defaultValue;

            return this.Values.Last();
        }
    }
}
