﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public class QueryFilter : IQueryFilter {
        public IEnumerable<IQueryFilterItem> Items { get; set; }
        public IQueryOperation Operation { get; set; }

        public QueryFilter(IEnumerable<IQueryFilterItem> items, IQueryOperation operation) {
            this.Items = items;
            this.Operation = operation;
        }

        public QueryFilter(IEnumerable<IQueryFilterItem> items)
            : this(items, null) {
        }

        public QueryFilter()
            : this(null) {
        }

        public IFilter<TModel> ToFilter<TModel>(Action<IQueryFilterMapper<TModel>> map) {
            var filter = this.Operation == null
                ? Filter<TModel>.Build(builder => {
                    map(new QueryFilterMapper<TModel>(this, builder));
                })
                : Filter<TModel>.Op(this.Operation ?? QueryOperation.And, builder => {
                    map(new QueryFilterMapper<TModel>(this, builder));
                });

            return filter ?? Filter<TModel>.ALL;
        }

        public static QueryFilter Parse(IEnumerable<KeyValuePair<string, StringValues>> query) {
            var items = new List<IQueryFilterItem>();
            var filter = new QueryFilter(items);
            foreach (var pair in query) {
                var key = pair.Key;
                var values = pair.Value;
                var match = Regex.Match(key, $"filter\\.(?<name>\\w+)(\\.(?<op>op))?", RegexOptions.IgnoreCase);
                if (match.Success) {
                    var name = match.Groups["name"].Value;
                    if (name.Equals("op", StringComparison.CurrentCultureIgnoreCase)) {
                        filter.Operation = QueryOperation.Parse(values.First());
                        continue;
                    }

                    var item = items.Find(x => x.Name.Equals(name)) as QueryFilterItem;
                    if (item == null) {
                        items.Add(item = new QueryFilterItem(name));
                    }

                    if (match.Groups["op"].Success) {
                        item.Operation = QueryOperation.Parse(values.First());
                        continue;
                    }

                    item.Values = values;
                }
            }
            return filter;
        }
    }
}
