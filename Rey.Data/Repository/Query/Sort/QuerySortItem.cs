﻿namespace Rey.Data {
    public class QuerySortItem : IQuerySortItem {
        public string Name { get; set; }
        public IQuerySortDirection Direction { get; set; }

        public QuerySortItem(string name, IQuerySortDirection direction) {
            this.Name = name;
            this.Direction = direction;
        }

        public QuerySortItem(string name)
            : this(name, null) {
        }

        public QuerySortItem()
            : this(null) {
        }
    }
}
