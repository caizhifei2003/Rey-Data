﻿namespace Rey.Data {
    public interface IQuerySortItem {
        string Name { get; }
        IQuerySortDirection Direction { get; }
    }
}
