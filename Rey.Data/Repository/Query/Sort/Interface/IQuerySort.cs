﻿using System;
using System.Collections.Generic;

namespace Rey.Data {
    public interface IQuerySort {
        IEnumerable<IQuerySortItem> Items { get; }
        ISort<TModel> ToSort<TModel>(Action<IQuerySortMapper<TModel>> map);
    }
}