﻿using System;

namespace Rey.Data {
    public interface IQuerySortMapper<TModel> {
        IQuerySortMapper<TModel> Map(string name, Action<IQuerySortItem, ISortBuilder<TModel>> build);
    }
}