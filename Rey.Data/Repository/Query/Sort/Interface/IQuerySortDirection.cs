﻿using System;

namespace Rey.Data {
    public interface IQuerySortDirection : IEquatable<IQuerySortDirection> {
        string Name { get; }
    }
}
