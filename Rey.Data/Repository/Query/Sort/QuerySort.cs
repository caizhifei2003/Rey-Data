﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public class QuerySort : IQuerySort {
        public IEnumerable<IQuerySortItem> Items { get; set; }

        public QuerySort(IEnumerable<IQuerySortItem> items) {
            this.Items = items;
        }

        public QuerySort()
            : this(null) {
        }

        public ISort<TModel> ToSort<TModel>(Action<IQuerySortMapper<TModel>> map) {
            return Sort<TModel>.Build(builder => {
                map(new QuerySortMapper<TModel>(this, builder));
            });
        }

        public static QuerySort Parse(IEnumerable<KeyValuePair<string, StringValues>> query) {
            var items = new List<IQuerySortItem>();
            var sort = new QuerySort(items);
            foreach (var pair in query) {
                var key = pair.Key;
                var values = pair.Value;
                var match = Regex.Match(key, $"sort\\.(?<name>\\w+)", RegexOptions.IgnoreCase);
                if (match.Success) {
                    var name = match.Groups["name"].Value;
                    items.Add(new QuerySortItem(name, QuerySortDirection.Parse(values.First())));
                }
            }
            return sort;
        }
    }
}
