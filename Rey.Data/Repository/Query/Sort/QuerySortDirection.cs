﻿using System;

namespace Rey.Data {
    public abstract class QuerySortDirection : IQuerySortDirection {
        public abstract string Name { get; }

        public bool Equals(IQuerySortDirection other) {
            return this.Name.Equals(other.Name);
        }

        public override string ToString() {
            return this.Name;
        }

        public static QuerySortDirection Asc { get; } = new AscendingQuerySortDirection();
        public static QuerySortDirection Desc { get; } = new DescendingQuerySortDirection();

        public static QuerySortDirection Parse(string name) {
            if (name.Equals("asc", StringComparison.CurrentCultureIgnoreCase))
                return Asc;

            if (name.Equals("desc", StringComparison.CurrentCultureIgnoreCase))
                return Desc;

            throw new InvalidOperationException($"invalid sort name {name}");
        }
    }
}
