﻿using System;
using System.Linq;

namespace Rey.Data {
    public class QuerySortMapper<TModel> : IQuerySortMapper<TModel> {
        private IQuerySort Sort { get; }
        private ISortBuilder<TModel> Builder { get; }

        public QuerySortMapper(IQuerySort sort, ISortBuilder<TModel> builder) {
            this.Sort = sort;
            this.Builder = builder;
        }

        public IQuerySortMapper<TModel> Map(string name, Action<IQuerySortItem, ISortBuilder<TModel>> build) {
            var item = this.Sort.Items.Where(x => x.Name.Equals(name)).FirstOrDefault();
            if (item == null)
                return this;

            build(item, this.Builder);
            return this;
        }
    }
}