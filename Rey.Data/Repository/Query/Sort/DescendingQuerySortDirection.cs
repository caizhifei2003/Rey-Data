﻿namespace Rey.Data {
    public class DescendingQuerySortDirection : QuerySortDirection {
        public override string Name => "Desc";
    }
}
