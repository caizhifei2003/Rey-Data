﻿namespace Rey.Data {
    public class AscendingQuerySortDirection : QuerySortDirection {
        public override string Name => "Asc";
    }
}
