﻿using System.Collections.Generic;
using System.Linq;

namespace Rey.Data.Repository {
    public class QueryResult<TModel> : IQueryResult<TModel> {
        public IEnumerable<TModel> Items { get; }
        public long Total { get; }
        public long Skip { get; }

        public QueryResult(IEnumerable<TModel> items, long total, long skip) {
            this.Items = items;
            this.Total = total;
            this.Skip = skip;
        }

        public QueryResult(IEnumerable<TModel> items, long total)
            : this(items, total, 0) {
        }

        public QueryResult(IEnumerable<TModel> items)
            : this(items, items.Count()) {
        }
    }
}
