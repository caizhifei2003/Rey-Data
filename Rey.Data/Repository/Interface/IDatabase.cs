﻿namespace Rey.Data {
    public interface IDatabase {
        IRepository<TModel> GetRepository<TModel>(string name = null);
        IModelRepository<TModel, TKey> GetModelRepository<TModel, TKey>(string name = null)
            where TModel : class, IModel<TKey>;
    }
}
