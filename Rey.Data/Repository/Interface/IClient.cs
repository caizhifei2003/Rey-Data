﻿namespace Rey.Data {
    public interface IClient {
        IDatabase GetDatabase(string name = null);
    }
}
