﻿namespace Rey.Data.Repository {
    public interface IQueryPage {
        long? Take { get; }
        long? Skip { get; }
    }
}
