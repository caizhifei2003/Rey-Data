﻿using System.Collections.Generic;

namespace Rey.Data {
    public interface IRepository<TModel> {
        TModel InsertOne(TModel model);
        TModel UpdateOne(IFilter<TModel> filter, IUpdate<TModel> update);
        TModel ReplaceOne(IFilter<TModel> filter, TModel model);
        TModel DeleteOne(IFilter<TModel> filter);
        TModel FindOne(IFilter<TModel> filter);

        long InsertMany(IEnumerable<TModel> models);
        long UpdateMany(IFilter<TModel> filter, IUpdate<TModel> update);
        long DeleteMany(IFilter<TModel> filter);
        IEnumerable<TModel> FindMany(IFilter<TModel> filter);

        long Count(IFilter<TModel> filter);

        QueryResult<TModel> Query(IFilter<TModel> filter, ISort<TModel> sort = null, IQueryPage page = null);
    }
}
