﻿namespace Rey.Data {
    public interface IModelRepository<TModel, TKey> : IRepository<TModel>
        where TModel : class, IModel<TKey> {
        TModel UpdateOne(TKey id, IUpdate<TModel> update);
        TModel ReplaceOne(TKey id, TModel model);
        TModel DeleteOne(TKey id);
        TModel FindOne(TKey id);
    }
}
