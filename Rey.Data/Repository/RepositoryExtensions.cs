﻿using System;
using System.Collections.Generic;

namespace Rey.Data {
    public static class RepositoryExtensions {
        public static TModel UpdateOne<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter, Action<IUpdateBuilder<TModel>> update) {
            return target.UpdateOne(
                Filter<TModel>.Build(filter),
                Update<TModel>.Build(update)
            );
        }

        public static TModel DeleteOne<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter) {
            return target.DeleteOne(Filter<TModel>.Build(filter));
        }

        public static TModel FindOne<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter) {
            return target.FindOne(Filter<TModel>.Build(filter));
        }

        public static long UpdateMany<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter, Action<IUpdateBuilder<TModel>> update) {
            return target.UpdateMany(
                Filter<TModel>.Build(filter),
                Update<TModel>.Build(update)
            );
        }

        public static long DeleteMany<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter) {
            return target.DeleteMany(Filter<TModel>.Build(filter));
        }

        public static IEnumerable<TModel> FindMany<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter) {
            return target.FindMany(Filter<TModel>.Build(filter));
        }

        public static long UpdateAll<TModel>(this IRepository<TModel> target, IUpdate<TModel> update) {
            return target.UpdateMany(Filter<TModel>.ALL, update);
        }

        public static long UpdateAll<TModel>(this IRepository<TModel> target, Action<IUpdateBuilder<TModel>> update) {
            return target.UpdateAll(Update<TModel>.Build(update));
        }

        public static long DeleteAll<TModel>(this IRepository<TModel> target) {
            return target.DeleteMany(Filter<TModel>.ALL);
        }

        public static IEnumerable<TModel> FindAll<TModel>(this IRepository<TModel> target) {
            return target.FindMany(Filter<TModel>.ALL);
        }

        public static long Count<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter) {
            return target.Count(Filter<TModel>.Build(filter));
        }

        public static bool Exist<TModel>(this IRepository<TModel> target, IFilter<TModel> filter) {
            return target.Count(filter) > 0;
        }

        public static bool Exist<TModel>(this IRepository<TModel> target, Action<IFilterBuilder<TModel>> filter) {
            return target.Count(filter) > 0;
        }

        public static IQueryResult<TModel> QueryAll<TModel>(this IRepository<TModel> target, ISort<TModel> sort = null, IQueryPage page = null) {
            return target.Query(Filter<TModel>.ALL, sort, page);
        }
    }
}
