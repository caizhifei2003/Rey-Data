﻿using System.Collections.Generic;
using System.Linq;

namespace Rey.Data {
    public class DefaultUpdateBuilder<TModel> : UpdateBuilder<TModel> {
        protected override IUpdate<TModel> Build(IEnumerable<IUpdate<TModel>> updates) {
            var count = updates.Count();
            if (count == 0)
                return null;

            if (count == 1)
                return updates.First();

            return new CombineUpdate<TModel>(updates);
        }
    }
}