﻿namespace Rey.Data {
    public interface IUpdate<TModel> {
        TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter);
    }
}