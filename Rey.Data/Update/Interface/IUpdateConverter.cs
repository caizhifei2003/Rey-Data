﻿namespace Rey.Data {
    public interface IUpdateConverter<TModel, TResult> {
        TResult Convert<TItem>(IAddToSetUpdate<TModel, TItem> update);
        TResult Convert<TItem>(IAddToSetEachUpdate<TModel, TItem> update);

        TResult Convert<TField>(IBitwiseAndUpdate<TModel, TField> update);
        TResult Convert<TField>(IBitwiseOrUpdate<TModel, TField> update);
        TResult Convert<TField>(IBitwiseXorUpdate<TModel, TField> update);

        TResult Convert(ICombineUpdate<TModel> update);
        TResult Convert(ICurrentDateUpdate<TModel> update);

        TResult Convert<TField>(IIncUpdate<TModel, TField> update);
        TResult Convert<TField>(IMaxUpdate<TModel, TField> update);
        TResult Convert<TField>(IMinUpdate<TModel, TField> update);
        TResult Convert<TField>(IMulUpdate<TModel, TField> update);

        TResult Convert(IPopFirstUpdate<TModel> update);
        TResult Convert(IPopLastUpdate<TModel> update);

        TResult Convert<TItem>(IPullUpdate<TModel, TItem> update);
        TResult Convert<TItem>(IPullAllUpdate<TModel, TItem> update);
        TResult Convert<TItem>(IPullFilterUpdate<TModel, TItem> update);
        TResult Convert<TItem>(IPushUpdate<TModel, TItem> update);
        TResult Convert<TItem>(IPushEachUpdate<TModel, TItem> update);

        TResult Convert(IRenameUpdate<TModel> update);

        TResult Convert<TField>(ISetUpdate<TModel, TField> update);
        TResult Convert<TField>(ISetOnInsertUpdate<TModel, TField> update);
        TResult Convert(IUnsetUpdate<TModel> update);
    }
}