﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IUpdateBuilder<TModel> {
        IUpdate<TModel> Build();

        IUpdateBuilder<TModel> AddToSet<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IUpdateBuilder<TModel> AddToSetEach<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);

        IUpdateBuilder<TModel> BitwiseAnd<TField>(Expression<Func<TModel, TField>> field, TField value);
        IUpdateBuilder<TModel> BitwiseOr<TField>(Expression<Func<TModel, TField>> field, TField value);
        IUpdateBuilder<TModel> BitwiseXor<TField>(Expression<Func<TModel, TField>> field, TField value);

        IUpdateBuilder<TModel> CurrentDate(Expression<Func<TModel, object>> field);

        IUpdateBuilder<TModel> Inc<TField>(Expression<Func<TModel, TField>> field, TField value);
        IUpdateBuilder<TModel> Max<TField>(Expression<Func<TModel, TField>> field, TField value);
        IUpdateBuilder<TModel> Min<TField>(Expression<Func<TModel, TField>> field, TField value);
        IUpdateBuilder<TModel> Mul<TField>(Expression<Func<TModel, TField>> field, TField value);

        IUpdateBuilder<TModel> PopFirst(Expression<Func<TModel, object>> field);
        IUpdateBuilder<TModel> PopLast(Expression<Func<TModel, object>> field);

        IUpdateBuilder<TModel> Pull<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IUpdateBuilder<TModel> PullAll<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);
        IUpdateBuilder<TModel> PullFilter<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Action<IFilterBuilder<TItem>> buildFilter);
        IUpdateBuilder<TModel> Push<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IUpdateBuilder<TModel> PushEach<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);

        IUpdateBuilder<TModel> Rename(Expression<Func<TModel, object>> field, string newName);

        IUpdateBuilder<TModel> Set<TField>(Expression<Func<TModel, TField>> field, TField value);
        IUpdateBuilder<TModel> SetOnInsert<TField>(Expression<Func<TModel, TField>> field, TField value);

        IUpdateBuilder<TModel> Unset(Expression<Func<TModel, object>> field);
    }
}