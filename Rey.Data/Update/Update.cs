﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public static class Update<TModel> {
        public static IUpdate<TModel> Build(Action<IUpdateBuilder<TModel>> build) {
            var builder = new DefaultUpdateBuilder<TModel>();
            build.Invoke(builder);
            return builder.Build();
        }

        public static IUpdate<TModel> AddToSet<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AddToSet(field, value));
        }

        public static IUpdate<TModel> AddToSetEach<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return Build(builder => builder.AddToSetEach(field, values));
        }

        public static IUpdate<TModel> BitwiseAnd<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.BitwiseAnd(field, value));
        }

        public static IUpdate<TModel> BitwiseOr<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.BitwiseOr(field, value));
        }

        public static IUpdate<TModel> BitwiseXor<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.BitwiseXor(field, value));
        }

        public static IUpdate<TModel> CurrentDate(Expression<Func<TModel, object>> field) {
            return Build(builder => builder.CurrentDate(field));
        }

        public static IUpdate<TModel> Inc<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Inc(field, value));
        }

        public static IUpdate<TModel> Max<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Max(field, value));
        }

        public static IUpdate<TModel> Min<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Min(field, value));
        }

        public static IUpdate<TModel> Mul<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Mul(field, value));
        }

        public static IUpdate<TModel> PopFirst(Expression<Func<TModel, object>> field) {
            return Build(builder => builder.PopFirst(field));
        }

        public static IUpdate<TModel> PopLast(Expression<Func<TModel, object>> field) {
            return Build(builder => builder.PopLast(field));
        }

        public static IUpdate<TModel> Pull<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.Pull(field, value));
        }

        public static IUpdate<TModel> PullAll<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return Build(builder => builder.PullAll(field, values));
        }

        public static IUpdate<TModel> PullFilter<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Action<IFilterBuilder<TItem>> buildFilter) {
            return Build(builder => builder.PullFilter(field, buildFilter));
        }

        public static IUpdate<TModel> Push<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.Push(field, value));
        }

        public static IUpdate<TModel> PushEach<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return Build(builder => builder.PushEach(field, values));
        }

        public static IUpdate<TModel> Rename(Expression<Func<TModel, object>> field, string newName) {
            return Build(builder => builder.Rename(field, newName));
        }

        public static IUpdate<TModel> Set<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Set(field, value));
        }

        public static IUpdate<TModel> SetOnInsert<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.SetOnInsert(field, value));
        }

        public static IUpdate<TModel> Unset(Expression<Func<TModel, object>> field) {
            return Build(builder => builder.Unset(field));
        }
    }
}