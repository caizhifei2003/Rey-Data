﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class PopLastUpdate<TModel> : IPopLastUpdate<TModel> {
        public Expression<Func<TModel, object>> Field { get; }

        public PopLastUpdate(Expression<Func<TModel, object>> field) {
            this.Field = field;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}