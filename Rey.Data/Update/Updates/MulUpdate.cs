﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class MulUpdate<TModel, TField> : IMulUpdate<TModel, TField> {
        public Expression<Func<TModel, TField>> Field { get; }
        public TField Value { get; }

        public MulUpdate(Expression<Func<TModel, TField>> field, TField value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}