﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class PullUpdate<TModel, TItem> : IPullUpdate<TModel, TItem> {
        public Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        public TItem Value { get; }

        public PullUpdate(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}