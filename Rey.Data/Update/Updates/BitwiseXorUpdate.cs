﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class BitwiseXorUpdate<TModel, TField> : IBitwiseXorUpdate<TModel, TField> {
        public Expression<Func<TModel, TField>> Field { get; }
        public TField Value { get; }

        public BitwiseXorUpdate(Expression<Func<TModel, TField>> field, TField value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}