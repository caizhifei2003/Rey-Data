﻿using System.Collections.Generic;

namespace Rey.Data {
    public class CombineUpdate<TModel> : ICombineUpdate<TModel> {
        public IEnumerable<IUpdate<TModel>> Updates { get; }

        public CombineUpdate(IEnumerable<IUpdate<TModel>> updates) {
            this.Updates = updates;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}