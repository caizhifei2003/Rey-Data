﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class PullFilterUpdate<TModel, TItem> : IPullFilterUpdate<TModel, TItem> {
        public Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        public IFilter<TItem> Filter { get; }

        public PullFilterUpdate(Expression<Func<TModel, IEnumerable<TItem>>> field, IFilter<TItem> filter) {
            this.Field = field;
            this.Filter = filter;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}