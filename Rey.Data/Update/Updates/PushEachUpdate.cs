﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class PushEachUpdate<TModel, TItem> : IPushEachUpdate<TModel, TItem> {
        public Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        public IEnumerable<TItem> Values { get; }

        public PushEachUpdate(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            this.Field = field;
            this.Values = values;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}