﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class RenameUpdate<TModel> : IRenameUpdate<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public string NewName { get; }

        public RenameUpdate(Expression<Func<TModel, object>> field, string newName) {
            this.Field = field;
            this.NewName = newName;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}