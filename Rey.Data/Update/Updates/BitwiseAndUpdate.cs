﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class BitwiseAndUpdate<TModel, TField> : IBitwiseAndUpdate<TModel, TField> {
        public Expression<Func<TModel, TField>> Field { get; }
        public TField Value { get; }

        public BitwiseAndUpdate(Expression<Func<TModel, TField>> field, TField value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IUpdateConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}