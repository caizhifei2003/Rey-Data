﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IAddToSetUpdate<TModel, TItem> : IUpdate<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }
}