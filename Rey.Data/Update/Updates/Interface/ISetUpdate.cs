﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface ISetUpdate<TModel, TField> : IUpdate<TModel> {
        Expression<Func<TModel, TField>> Field { get; }
        TField Value { get; }
    }
}