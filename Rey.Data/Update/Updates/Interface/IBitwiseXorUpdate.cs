﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IBitwiseXorUpdate<TModel, TField> : IUpdate<TModel> {
        Expression<Func<TModel, TField>> Field { get; }
        TField Value { get; }
    }
}