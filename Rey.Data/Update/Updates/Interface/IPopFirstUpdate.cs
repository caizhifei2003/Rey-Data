﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IPopFirstUpdate<TModel> : IUpdate<TModel> {
        Expression<Func<TModel, object>> Field { get; }
    }
}