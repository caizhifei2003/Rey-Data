﻿using System.Collections.Generic;

namespace Rey.Data {
    public interface ICombineUpdate<TModel> : IUpdate<TModel> {
        IEnumerable<IUpdate<TModel>> Updates { get; }
    }
}