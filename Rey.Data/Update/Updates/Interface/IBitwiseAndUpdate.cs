﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IBitwiseAndUpdate<TModel, TField> : IUpdate<TModel> {
        Expression<Func<TModel, TField>> Field { get; }
        TField Value { get; }
    }
}