﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IAddToSetEachUpdate<TModel, TItem> : IUpdate<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        IEnumerable<TItem> Values { get; }
    }
}