﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IPopLastUpdate<TModel> : IUpdate<TModel> {
        Expression<Func<TModel, object>> Field { get; }
    }
}