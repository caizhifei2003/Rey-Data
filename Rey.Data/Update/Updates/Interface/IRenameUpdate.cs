﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IRenameUpdate<TModel> : IUpdate<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        string NewName { get; }
    }
}