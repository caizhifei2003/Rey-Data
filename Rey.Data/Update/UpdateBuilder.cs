﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public abstract class UpdateBuilder<TModel> : IUpdateBuilder<TModel> {
        private List<IUpdate<TModel>> Updates { get; } = new List<IUpdate<TModel>>();

        protected virtual IUpdateBuilder<TModel> OnUpdate(IUpdate<TModel> update) {
            this.Updates.Add(update);
            return this;
        }

        public IUpdate<TModel> Build() {
            return this.Build(this.Updates);
        }

        protected abstract IUpdate<TModel> Build(IEnumerable<IUpdate<TModel>> updates);

        public IUpdateBuilder<TModel> AddToSet<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnUpdate(new AddToSetUpdate<TModel, TItem>(field, value));
        }

        public IUpdateBuilder<TModel> AddToSetEach<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return this.OnUpdate(new AddToSetEachUpdate<TModel, TItem>(field, values));
        }

        public IUpdateBuilder<TModel> BitwiseAnd<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new BitwiseAndUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> BitwiseOr<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new BitwiseOrUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> BitwiseXor<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new BitwiseXorUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> CurrentDate(Expression<Func<TModel, object>> field) {
            return this.OnUpdate(new CurrentDateUpdate<TModel>(field));
        }

        public IUpdateBuilder<TModel> Inc<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new IncUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> Max<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new MaxUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> Min<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new MinUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> Mul<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new MulUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> PopFirst(Expression<Func<TModel, object>> field) {
            return this.OnUpdate(new PopFirstUpdate<TModel>(field));
        }

        public IUpdateBuilder<TModel> PopLast(Expression<Func<TModel, object>> field) {
            return this.OnUpdate(new PopLastUpdate<TModel>(field));
        }

        public IUpdateBuilder<TModel> Pull<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnUpdate(new PullUpdate<TModel, TItem>(field, value));
        }

        public IUpdateBuilder<TModel> PullAll<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return this.OnUpdate(new PullAllUpdate<TModel, TItem>(field, values));
        }

        public IUpdateBuilder<TModel> PullFilter<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Action<IFilterBuilder<TItem>> buildFilter) {
            return this.OnUpdate(new PullFilterUpdate<TModel, TItem>(field, Filter<TItem>.Build(buildFilter)));
        }

        public IUpdateBuilder<TModel> Push<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnUpdate(new PushUpdate<TModel, TItem>(field, value));
        }

        public IUpdateBuilder<TModel> PushEach<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return this.OnUpdate(new PushEachUpdate<TModel, TItem>(field, values));
        }

        public IUpdateBuilder<TModel> Rename(Expression<Func<TModel, object>> field, string newName) {
            return this.OnUpdate(new RenameUpdate<TModel>(field, newName));
        }

        public IUpdateBuilder<TModel> Set<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new SetUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> SetOnInsert<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnUpdate(new SetOnInsertUpdate<TModel, TField>(field, value));
        }

        public IUpdateBuilder<TModel> Unset(Expression<Func<TModel, object>> field) {
            return this.OnUpdate(new UnsetUpdate<TModel>(field));
        }
    }
}