﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public static class Filter<TModel> {
        public static readonly IFilter<TModel> ALL = Where(x => true);

        public static IFilter<TModel> Build(Action<IFilterBuilder<TModel>> build) {
            var builder = new DefaultFilterBuilder<TModel>();
            build.Invoke(builder);
            return builder.Build();
        }

        public static IFilter<TModel> Not(IFilter<TModel> filter) {
            return new NotFilter<TModel>(filter);
        }

        public static IFilter<TModel> Not(Action<IFilterBuilder<TModel>> build) {
            return Not(Build(build));
        }

        public static IFilter<TModel> And(IEnumerable<IFilter<TModel>> filters) {
            return new AndFilter<TModel>(filters);
        }

        public static IFilter<TModel> And(params IFilter<TModel>[] filters) {
            return And(filters.ToList());
        }

        public static IFilter<TModel> And(Action<IFilterBuilder<TModel>> build) {
            var builder = new AndFilterBuilder<TModel>();
            build.Invoke(builder);
            return builder.Build();
        }

        public static IFilter<TModel> Or(IEnumerable<IFilter<TModel>> filters) {
            return new OrFilter<TModel>(filters);
        }

        public static IFilter<TModel> Or(params IFilter<TModel>[] filters) {
            return Or(filters.ToList());
        }

        public static IFilter<TModel> Or(Action<IFilterBuilder<TModel>> build) {
            var builder = new OrFilterBuilder<TModel>();
            build.Invoke(builder);
            return builder.Build();
        }

        public static IFilter<TModel> Op(IQueryOperation operation, IEnumerable<IFilter<TModel>> filters) {
            if (operation.Equals(QueryOperation.And))
                return And(filters);

            if (operation.Equals(QueryOperation.Or))
                return Or(filters);

            throw new InvalidOperationException($"invalid operation {operation}");
        }

        public static IFilter<TModel> Op(IQueryOperation operation, params IFilter<TModel>[] filters) {
            if (operation.Equals(QueryOperation.And))
                return And(filters);

            if (operation.Equals(QueryOperation.Or))
                return Or(filters);

            throw new InvalidOperationException($"invalid operation {operation}");
        }

        public static IFilter<TModel> Op(IQueryOperation operation, Action<IFilterBuilder<TModel>> build) {
            if (operation.Equals(QueryOperation.And))
                return And(build);

            if (operation.Equals(QueryOperation.Or))
                return Or(build);

            throw new InvalidOperationException($"invalid operation {operation}");
        }

        public static IFilter<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Eq(field, value));
        }

        public static IFilter<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Ne(field, value));
        }

        public static IFilter<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Gt(field, value));
        }

        public static IFilter<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Gte(field, value));
        }

        public static IFilter<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Lt(field, value));
        }

        public static IFilter<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return Build(builder => builder.Lte(field, value));
        }

        public static IFilter<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AnyEq(field, value));
        }

        public static IFilter<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AnyNe(field, value));
        }

        public static IFilter<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AnyGt(field, value));
        }

        public static IFilter<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AnyGte(field, value));
        }

        public static IFilter<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AnyLt(field, value));
        }

        public static IFilter<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return Build(builder => builder.AnyLte(field, value));
        }

        public static IFilter<TModel> Size(Expression<Func<TModel, object>> field, int size) {
            return Build(builder => builder.Size(field, size));
        }

        public static IFilter<TModel> SizeGt(Expression<Func<TModel, object>> field, int size) {
            return Build(builder => builder.SizeGt(field, size));
        }

        public static IFilter<TModel> SizeGte(Expression<Func<TModel, object>> field, int size) {
            return Build(builder => builder.SizeGte(field, size));
        }

        public static IFilter<TModel> SizeLt(Expression<Func<TModel, object>> field, int size) {
            return Build(builder => builder.SizeLt(field, size));
        }

        public static IFilter<TModel> SizeLte(Expression<Func<TModel, object>> field, int size) {
            return Build(builder => builder.SizeLte(field, size));
        }

        public static IFilter<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return Build(builder => builder.In(field, values));
        }

        public static IFilter<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return Build(builder => builder.Nin(field, values));
        }

        public static IFilter<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return Build(builder => builder.All(field, values));
        }

        public static IFilter<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return Build(builder => builder.AnyIn(field, values));
        }

        public static IFilter<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return Build(builder => builder.AnyNin(field, values));
        }

        public static IFilter<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Action<IFilterBuilder<TItem>> build) {
            return Build(builder => builder.ElemMatch(field, build));
        }

        public static IFilter<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask) {
            return Build(builder => builder.BitsAllClear(field, bitmask));
        }

        public static IFilter<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask) {
            return Build(builder => builder.BitsAllSet(field, bitmask));
        }

        public static IFilter<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask) {
            return Build(builder => builder.BitsAnyClear(field, bitmask));
        }

        public static IFilter<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask) {
            return Build(builder => builder.BitsAnySet(field, bitmask));
        }

        public static IFilter<TModel> Exists(Expression<Func<TModel, object>> field, bool exists) {
            return Build(builder => builder.Exists(field, exists));
        }

        public static IFilter<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            return Build(builder => builder.Mod(field, modulus, remainder));
        }

        public static IFilter<TModel> Regex(Expression<Func<TModel, object>> field, Regex regex) {
            return Build(builder => builder.Regex(field, regex));
        }

        public static IFilter<TModel> Regex(Expression<Func<TModel, object>> field, string value, bool ignoreCase = true) {
            return Build(builder => builder.Regex(field, value, ignoreCase));
        }

        public static IFilter<TModel> RegexAny(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex) {
            return Build(builder => builder.RegexAny(field, regex));
        }

        public static IFilter<TModel> RegexAny(Expression<Func<TModel, object>> field, IEnumerable<string> values, bool ignoreCase = true) {
            return Build(builder => builder.RegexAny(field, values, ignoreCase));
        }

        public static IFilter<TModel> RegexAll(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex) {
            return Build(builder => builder.RegexAll(field, regex));
        }

        public static IFilter<TModel> RegexAll(Expression<Func<TModel, object>> field, IEnumerable<string> values, bool ignoreCase = true) {
            return Build(builder => builder.RegexAll(field, values, ignoreCase));
        }

        public static IFilter<TModel> Text(string search, string language = null) {
            return Build(builder => builder.Text(search, language));
        }

        public static IFilter<TModel> Where(Expression<Func<TModel, bool>> expression) {
            return Build(builder => builder.Where(expression));
        }
    }
}
