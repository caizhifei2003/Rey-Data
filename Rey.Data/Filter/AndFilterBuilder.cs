﻿using System.Collections.Generic;

namespace Rey.Data {
    public class AndFilterBuilder<TModel> : CompositionFilterBuilder<TModel> {
        protected override IFilter<TModel> Build(IEnumerable<IFilter<TModel>> filters) {
            return new AndFilter<TModel>(filters);
        }
    }
}
