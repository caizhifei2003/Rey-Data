﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public abstract class FilterBuilder<TModel> : IFilterBuilder<TModel> {
        public abstract IFilter<TModel> Build();
        protected abstract IFilterBuilder<TModel> OnFilter(IFilter<TModel> filter);

        public IFilterBuilder<TModel> Not(IFilter<TModel> filter) {
            return this.OnFilter(Filter<TModel>.Not(filter));
        }

        public IFilterBuilder<TModel> Not(Action<IFilterBuilder<TModel>> build) {
            return this.OnFilter(Filter<TModel>.Not(build));
        }

        public IFilterBuilder<TModel> And(IEnumerable<IFilter<TModel>> filters) {
            return this.OnFilter(Filter<TModel>.And(filters));
        }

        public IFilterBuilder<TModel> And(params IFilter<TModel>[] filters) {
            return this.OnFilter(Filter<TModel>.And(filters));
        }

        public IFilterBuilder<TModel> And(Action<IFilterBuilder<TModel>> build) {
            return this.OnFilter(Filter<TModel>.And(build));
        }

        public IFilterBuilder<TModel> Or(IEnumerable<IFilter<TModel>> filters) {
            return this.OnFilter(Filter<TModel>.Or(filters));
        }

        public IFilterBuilder<TModel> Or(params IFilter<TModel>[] filters) {
            return this.OnFilter(Filter<TModel>.Or(filters));
        }

        public IFilterBuilder<TModel> Or(Action<IFilterBuilder<TModel>> build) {
            return this.OnFilter(Filter<TModel>.Or(build));
        }

        public IFilterBuilder<TModel> Op(IQueryOperation operation, IEnumerable<IFilter<TModel>> filters) {
            return this.OnFilter(Filter<TModel>.Op(operation, filters));
        }

        public IFilterBuilder<TModel> Op(IQueryOperation operation, params IFilter<TModel>[] filters) {
            return this.OnFilter(Filter<TModel>.Op(operation, filters));
        }

        public IFilterBuilder<TModel> Op(IQueryOperation operation, Action<IFilterBuilder<TModel>> build) {
            return this.OnFilter(Filter<TModel>.Op(operation, build));
        }

        public IFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnFilter(new EqFilter<TModel, TField>(field, value));
        }

        public IFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnFilter(new NeFilter<TModel, TField>(field, value));
        }

        public IFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnFilter(new GtFilter<TModel, TField>(field, value));
        }

        public IFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnFilter(new GteFilter<TModel, TField>(field, value));
        }

        public IFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnFilter(new LtFilter<TModel, TField>(field, value));
        }

        public IFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value) {
            return this.OnFilter(new LteFilter<TModel, TField>(field, value));
        }

        public IFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnFilter(new AnyEqFilter<TModel, TItem>(field, value));
        }

        public IFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnFilter(new AnyNeFilter<TModel, TItem>(field, value));
        }

        public IFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnFilter(new AnyGtFilter<TModel, TItem>(field, value));
        }

        public IFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnFilter(new AnyGteFilter<TModel, TItem>(field, value));
        }

        public IFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnFilter(new AnyLtFilter<TModel, TItem>(field, value));
        }

        public IFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            return this.OnFilter(new AnyLteFilter<TModel, TItem>(field, value));
        }

        public IFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size) {
            return this.OnFilter(new SizeFilter<TModel>(field, size));
        }

        public IFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size) {
            return this.OnFilter(new SizeGtFilter<TModel>(field, size));
        }

        public IFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size) {
            return this.OnFilter(new SizeGteFilter<TModel>(field, size));
        }

        public IFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size) {
            return this.OnFilter(new SizeLtFilter<TModel>(field, size));
        }

        public IFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size) {
            return this.OnFilter(new SizeLteFilter<TModel>(field, size));
        }

        public IFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return this.OnFilter(new InFilter<TModel, TField>(field, values));
        }

        public IFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            return this.OnFilter(new NinFilter<TModel, TField>(field, values));
        }

        public IFilterBuilder<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return this.OnFilter(new AllFilter<TModel, TItem>(field, values));
        }

        public IFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return this.OnFilter(new AnyInFilter<TModel, TItem>(field, values));
        }

        public IFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            return this.OnFilter(new AnyNinFilter<TModel, TItem>(field, values));
        }

        public IFilterBuilder<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Action<IFilterBuilder<TItem>> build) {
            return this.OnFilter(new ElemMatchFilter<TModel, TItem>(field, Filter<TItem>.Build(build)));
        }

        public IFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask) {
            return this.OnFilter(new BitsAllClearFilter<TModel>(field, bitmask));
        }

        public IFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask) {
            return this.OnFilter(new BitsAllSetFilter<TModel>(field, bitmask));
        }

        public IFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask) {
            return this.OnFilter(new BitsAnyClearFilter<TModel>(field, bitmask));
        }

        public IFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask) {
            return this.OnFilter(new BitsAnySetFilter<TModel>(field, bitmask));
        }

        public IFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists) {
            return this.OnFilter(new ExistsFilter<TModel>(field, exists));
        }

        public IFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            return this.OnFilter(new ModFilter<TModel>(field, modulus, remainder));
        }

        public IFilterBuilder<TModel> Regex(Expression<Func<TModel, object>> field, Regex regex) {
            return this.OnFilter(new RegexFilter<TModel>(field, regex));
        }

        public IFilterBuilder<TModel> Regex(Expression<Func<TModel, object>> field, string value, bool ignoreCase = true) {
            return this.OnFilter(new RegexFilter<TModel>(field, GenerateRegex(value, ignoreCase)));
        }

        public IFilterBuilder<TModel> RegexAny(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex) {
            return this.OnFilter(new RegexAnyFilter<TModel>(field, regex));
        }

        public IFilterBuilder<TModel> RegexAny(Expression<Func<TModel, object>> field, IEnumerable<string> values, bool ignoreCase = true) {
            return this.OnFilter(new RegexAnyFilter<TModel>(field, values.Select(value => GenerateRegex(value, ignoreCase))));
        }

        public IFilterBuilder<TModel> RegexAll(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex) {
            return this.OnFilter(new RegexAllFilter<TModel>(field, regex));
        }

        public IFilterBuilder<TModel> RegexAll(Expression<Func<TModel, object>> field, IEnumerable<string> values, bool ignoreCase = true) {
            return this.OnFilter(new RegexAllFilter<TModel>(field, values.Select(value => GenerateRegex(value, ignoreCase))));
        }

        public IFilterBuilder<TModel> Text(string search, string language = null) {
            return this.OnFilter(new TextFilter<TModel>(search, language));
        }

        public IFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression) {
            return this.OnFilter(new WhereFilter<TModel>(expression));
        }

        private static Regex GenerateRegex(string value, bool ignoreCase) {
            var options = RegexOptions.None;
            if (ignoreCase)
                options = options | RegexOptions.IgnoreCase;

            return new Regex(System.Text.RegularExpressions.Regex.Escape(value), options);
        }

    }
}
