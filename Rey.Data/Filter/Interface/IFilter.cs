﻿namespace Rey.Data {
    public interface IFilter<TModel> {
        TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter);
    }
}
