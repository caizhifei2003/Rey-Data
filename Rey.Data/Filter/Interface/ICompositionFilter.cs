﻿using System.Collections.Generic;

namespace Rey.Data {
    public interface ICompositionFilter<TModel> : IFilter<TModel> {
        IEnumerable<IFilter<TModel>> Filters { get; }
    }
}
