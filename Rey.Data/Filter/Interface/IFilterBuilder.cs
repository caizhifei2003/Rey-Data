﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public interface IFilterBuilder<TModel> {
        IFilter<TModel> Build();
        IFilterBuilder<TModel> Not(IFilter<TModel> filter);
        IFilterBuilder<TModel> Not(Action<IFilterBuilder<TModel>> build);
        IFilterBuilder<TModel> And(IEnumerable<IFilter<TModel>> filters);
        IFilterBuilder<TModel> And(params IFilter<TModel>[] filters);
        IFilterBuilder<TModel> And(Action<IFilterBuilder<TModel>> build);
        IFilterBuilder<TModel> Or(IEnumerable<IFilter<TModel>> filters);
        IFilterBuilder<TModel> Or(params IFilter<TModel>[] filters);
        IFilterBuilder<TModel> Or(Action<IFilterBuilder<TModel>> build);
        IFilterBuilder<TModel> Op(IQueryOperation operation, IEnumerable<IFilter<TModel>> filters);
        IFilterBuilder<TModel> Op(IQueryOperation operation, params IFilter<TModel>[] filters);
        IFilterBuilder<TModel> Op(IQueryOperation operation, Action<IFilterBuilder<TModel>> build);

        IFilterBuilder<TModel> Eq<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Ne<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Gt<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Gte<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Lt<TField>(Expression<Func<TModel, TField>> field, TField value);
        IFilterBuilder<TModel> Lte<TField>(Expression<Func<TModel, TField>> field, TField value);

        IFilterBuilder<TModel> AnyEq<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyNe<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyGt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyGte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyLt<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);
        IFilterBuilder<TModel> AnyLte<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value);

        IFilterBuilder<TModel> Size(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeGt(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeGte(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeLt(Expression<Func<TModel, object>> field, int size);
        IFilterBuilder<TModel> SizeLte(Expression<Func<TModel, object>> field, int size);

        IFilterBuilder<TModel> In<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values);
        IFilterBuilder<TModel> Nin<TField>(Expression<Func<TModel, TField>> field, IEnumerable<TField> values);

        IFilterBuilder<TModel> All<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);
        IFilterBuilder<TModel> AnyIn<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);
        IFilterBuilder<TModel> AnyNin<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values);

        IFilterBuilder<TModel> ElemMatch<TItem>(Expression<Func<TModel, IEnumerable<TItem>>> field, Action<IFilterBuilder<TItem>> build);

        IFilterBuilder<TModel> BitsAllClear(Expression<Func<TModel, object>> field, long bitmask);
        IFilterBuilder<TModel> BitsAllSet(Expression<Func<TModel, object>> field, long bitmask);
        IFilterBuilder<TModel> BitsAnyClear(Expression<Func<TModel, object>> field, long bitmask);
        IFilterBuilder<TModel> BitsAnySet(Expression<Func<TModel, object>> field, long bitmask);

        IFilterBuilder<TModel> Exists(Expression<Func<TModel, object>> field, bool exists);
        IFilterBuilder<TModel> Mod(Expression<Func<TModel, object>> field, long modulus, long remainder);

        IFilterBuilder<TModel> Regex(Expression<Func<TModel, object>> field, Regex regex);
        IFilterBuilder<TModel> Regex(Expression<Func<TModel, object>> field, string value, bool ignoreCase = true);
        IFilterBuilder<TModel> RegexAny(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex);
        IFilterBuilder<TModel> RegexAny(Expression<Func<TModel, object>> field, IEnumerable<string> values, bool ignoreCase = true);
        IFilterBuilder<TModel> RegexAll(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex);
        IFilterBuilder<TModel> RegexAll(Expression<Func<TModel, object>> field, IEnumerable<string> values, bool ignoreCase = true);

        IFilterBuilder<TModel> Text(string search, string language = null);

        IFilterBuilder<TModel> Where(Expression<Func<TModel, bool>> expression);
    }
}
