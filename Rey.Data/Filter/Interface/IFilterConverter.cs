﻿namespace Rey.Data {
    public interface IFilterConverter<TModel, TResult> {
        TResult Convert(INotFilter<TModel> filter);
        TResult Convert(IAndFilter<TModel> filter);
        TResult Convert(IOrFilter<TModel> filter);

        TResult Convert<TField>(IEqFilter<TModel, TField> filter);
        TResult Convert<TField>(INeFilter<TModel, TField> filter);
        TResult Convert<TField>(IGtFilter<TModel, TField> filter);
        TResult Convert<TField>(IGteFilter<TModel, TField> filter);
        TResult Convert<TField>(ILtFilter<TModel, TField> filter);
        TResult Convert<TField>(ILteFilter<TModel, TField> filter);

        TResult Convert<TItem>(IAnyEqFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyNeFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyGtFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyGteFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyLtFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyLteFilter<TModel, TItem> filter);

        TResult Convert(ISizeFilter<TModel> filter);
        TResult Convert(ISizeGtFilter<TModel> filter);
        TResult Convert(ISizeGteFilter<TModel> filter);
        TResult Convert(ISizeLtFilter<TModel> filter);
        TResult Convert(ISizeLteFilter<TModel> filter);

        TResult Convert<TField>(IInFilter<TModel, TField> filter);
        TResult Convert<TField>(INinFilter<TModel, TField> filter);

        TResult Convert<TItem>(IAllFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyInFilter<TModel, TItem> filter);
        TResult Convert<TItem>(IAnyNinFilter<TModel, TItem> filter);

        TResult Convert<TItem>(IElemMatchFilter<TModel, TItem> filter);

        TResult Convert(IBitsAllClearFilter<TModel> filter);
        TResult Convert(IBitsAllSetFilter<TModel> filter);
        TResult Convert(IBitsAnyClearFilter<TModel> filter);
        TResult Convert(IBitsAnySetFilter<TModel> filter);

        TResult Convert(IExistsFilter<TModel> filter);
        TResult Convert(IModFilter<TModel> filter);

        TResult Convert(IRegexFilter<TModel> filter);
        TResult Convert(IRegexAnyFilter<TModel> filter);
        TResult Convert(IRegexAllFilter<TModel> filter);

        TResult Convert(ITextFilter<TModel> filter);

        TResult Convert(IWhereFilter<TModel> filter);
    }
}
