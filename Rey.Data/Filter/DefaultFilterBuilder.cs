﻿using System;

namespace Rey.Data {
    public class DefaultFilterBuilder<TModel> : FilterBuilder<TModel>, IFilterBuilder<TModel> {
        private IFilter<TModel> Filter { get; set; }

        public override IFilter<TModel> Build() {
            return this.Filter;
        }

        protected override IFilterBuilder<TModel> OnFilter(IFilter<TModel> filter) {
            if (this.Filter != null)
                throw new InvalidOperationException("filter has been set");

            this.Filter = filter;
            return this;
        }
    }
}
