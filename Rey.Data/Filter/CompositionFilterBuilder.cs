﻿using System.Collections.Generic;

namespace Rey.Data {
    public abstract class CompositionFilterBuilder<TModel> : FilterBuilder<TModel>, IFilterBuilder<TModel> {
        private List<IFilter<TModel>> Filters { get; } = new List<IFilter<TModel>>();

        protected override IFilterBuilder<TModel> OnFilter(IFilter<TModel> filter) {
            this.Filters.Add(filter);
            return this;
        }

        public override IFilter<TModel> Build() {
            return this.Build(this.Filters);
        }

        protected abstract IFilter<TModel> Build(IEnumerable<IFilter<TModel>> filters);
    }
}
