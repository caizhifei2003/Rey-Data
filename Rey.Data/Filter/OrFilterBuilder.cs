﻿using System.Collections.Generic;

namespace Rey.Data {
    public class OrFilterBuilder<TModel> : CompositionFilterBuilder<TModel> {
        protected override IFilter<TModel> Build(IEnumerable<IFilter<TModel>> filters) {
            return new OrFilter<TModel>(filters);
        }
    }
}
