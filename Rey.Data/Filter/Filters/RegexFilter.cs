﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public class RegexFilter<TModel> : IRegexFilter<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public Regex Regex { get; }

        public RegexFilter(Expression<Func<TModel, object>> field, Regex regex) {
            this.Field = field;
            this.Regex = regex;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
