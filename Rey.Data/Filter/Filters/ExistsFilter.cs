﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class ExistsFilter<TModel> : IExistsFilter<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public bool Exists { get; }

        public ExistsFilter(Expression<Func<TModel, object>> field, bool exists) {
            this.Field = field;
            this.Exists = exists;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
