﻿namespace Rey.Data {
    public class NotFilter<TModel> : INotFilter<TModel> {
        public IFilter<TModel> Filter { get; }

        public NotFilter(IFilter<TModel> filter) {
            this.Filter = filter;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
