﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class WhereFilter<TModel> : IWhereFilter<TModel> {
        public Expression<Func<TModel, bool>> Expresson { get; }

        public WhereFilter(Expression<Func<TModel, bool>> expression) {
            this.Expresson = expression;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
