﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class AnyNinFilter<TModel, TItem> : IAnyNinFilter<TModel, TItem> {
        public Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        public IEnumerable<TItem> Values { get; }

        public AnyNinFilter(Expression<Func<TModel, IEnumerable<TItem>>> field, IEnumerable<TItem> values) {
            this.Field = field;
            this.Values = values;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
