﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public class RegexAllFilter<TModel> : IRegexAllFilter<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public IEnumerable<Regex> Regex { get; }

        public RegexAllFilter(Expression<Func<TModel, object>> field, IEnumerable<Regex> regex) {
            this.Field = field;
            this.Regex = regex;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
