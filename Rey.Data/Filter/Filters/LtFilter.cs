﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class LtFilter<TModel, TField> : ILtFilter<TModel, TField> {
        public Expression<Func<TModel, TField>> Field { get; }
        public TField Value { get; }

        public LtFilter(Expression<Func<TModel, TField>> field, TField value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
