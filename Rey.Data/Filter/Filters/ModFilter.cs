﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class ModFilter<TModel> : IModFilter<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public long Modulus { get; }
        public long Remainder { get; }

        public ModFilter(Expression<Func<TModel, object>> field, long modulus, long remainder) {
            this.Field = field;
            this.Modulus = modulus;
            this.Remainder = remainder;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
