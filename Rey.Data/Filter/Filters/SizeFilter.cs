﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class SizeFilter<TModel> : ISizeFilter<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public int Size { get; }

        public SizeFilter(Expression<Func<TModel, object>> field, int size) {
            this.Field = field;
            this.Size = size;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
