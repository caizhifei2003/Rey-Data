﻿namespace Rey.Data {
    public class TextFilter<TModel> : ITextFilter<TModel> {
        public string Search { get; }
        public string Language { get; }

        public TextFilter(string search, string language = null) {
            this.Search = search;
            this.Language = language;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
