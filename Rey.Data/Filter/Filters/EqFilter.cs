﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class EqFilter<TModel, TField> : IEqFilter<TModel, TField> {
        public Expression<Func<TModel, TField>> Field { get; }
        public TField Value { get; }

        public EqFilter(Expression<Func<TModel, TField>> field, TField value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
