﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class AnyLtFilter<TModel, TItem> : IAnyLtFilter<TModel, TItem> {
        public Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        public TItem Value { get; }

        public AnyLtFilter(Expression<Func<TModel, IEnumerable<TItem>>> field, TItem value) {
            this.Field = field;
            this.Value = value;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
