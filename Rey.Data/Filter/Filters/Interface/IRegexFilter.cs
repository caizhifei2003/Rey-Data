﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public interface IRegexFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        Regex Regex { get; }
    }
}
