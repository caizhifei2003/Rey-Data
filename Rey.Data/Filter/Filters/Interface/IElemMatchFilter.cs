﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IElemMatchFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        IFilter<TItem> Filter { get; }
    }
}
