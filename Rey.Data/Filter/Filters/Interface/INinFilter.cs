﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface INinFilter<TModel, TField> : IFilter<TModel> {
        Expression<Func<TModel, TField>> Field { get; }
        IEnumerable<TField> Values { get; }
    }
}
