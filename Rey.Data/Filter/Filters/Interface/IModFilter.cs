﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IModFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        long Modulus { get; }
        long Remainder { get; }
    }
}
