﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IWhereFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, bool>> Expresson { get; }
    }
}
