﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IAnyEqFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }

    public interface IAnyNeFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }

    public interface IAnyGtFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }

    public interface IAnyGteFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }

    public interface IAnyLtFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }

    public interface IAnyLteFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        TItem Value { get; }
    }
}
