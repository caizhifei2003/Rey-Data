﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IEqFilter<TModel, TField> : IFilter<TModel> {
        Expression<Func<TModel, TField>> Field { get; }
        TField Value { get; }
    }
}
