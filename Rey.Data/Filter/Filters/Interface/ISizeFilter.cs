﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface ISizeFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        int Size { get; }
    }
}
