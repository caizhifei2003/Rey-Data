﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Data {
    public interface IRegexAnyFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        IEnumerable<Regex> Regex { get; }
    }
}
