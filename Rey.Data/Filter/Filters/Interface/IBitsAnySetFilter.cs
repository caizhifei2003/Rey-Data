﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IBitsAnySetFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        long Bitmask { get; }
    }
}
