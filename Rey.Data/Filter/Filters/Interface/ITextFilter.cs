﻿namespace Rey.Data {
    public interface ITextFilter<TModel> : IFilter<TModel> {
        string Search { get; }
        string Language { get; }
    }
}
