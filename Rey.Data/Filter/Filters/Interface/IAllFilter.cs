﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IAllFilter<TModel, TItem> : IFilter<TModel> {
        Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        IEnumerable<TItem> Values { get; }
    }
}
