﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IGtFilter<TModel, TField> : IFilter<TModel> {
        Expression<Func<TModel, TField>> Field { get; }
        TField Value { get; }
    }
}
