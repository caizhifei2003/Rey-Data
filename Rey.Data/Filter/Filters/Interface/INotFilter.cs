﻿namespace Rey.Data {
    public interface INotFilter<TModel> : IFilter<TModel> {
        IFilter<TModel> Filter { get; }
    }
}
