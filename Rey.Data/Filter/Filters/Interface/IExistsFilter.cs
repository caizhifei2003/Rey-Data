﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IExistsFilter<TModel> : IFilter<TModel> {
        Expression<Func<TModel, object>> Field { get; }
        bool Exists { get; }
    }
}
