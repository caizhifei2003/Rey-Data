﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class BitsAnyClearFilter<TModel> : IBitsAnyClearFilter<TModel> {
        public Expression<Func<TModel, object>> Field { get; }
        public long Bitmask { get; }

        public BitsAnyClearFilter(Expression<Func<TModel, object>> field, long bitmask) {
            this.Field = field;
            this.Bitmask = bitmask;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
