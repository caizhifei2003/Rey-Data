﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class NinFilter<TModel, TField> : INinFilter<TModel, TField> {
        public Expression<Func<TModel, TField>> Field { get; }
        public IEnumerable<TField> Values { get; }

        public NinFilter(Expression<Func<TModel, TField>> field, IEnumerable<TField> values) {
            this.Field = field;
            this.Values = values;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
