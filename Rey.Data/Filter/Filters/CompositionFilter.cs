﻿using System.Collections.Generic;

namespace Rey.Data {
    public abstract class CompositionFilter<TModel> : ICompositionFilter<TModel> {
        public IEnumerable<IFilter<TModel>> Filters { get; }

        public CompositionFilter(IEnumerable<IFilter<TModel>> filters) {
            this.Filters = filters;
        }

        public abstract TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter);
    }
}
