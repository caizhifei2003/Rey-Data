﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public class ElemMatchFilter<TModel, TItem> : IElemMatchFilter<TModel, TItem> {
        public Expression<Func<TModel, IEnumerable<TItem>>> Field { get; }
        public IFilter<TItem> Filter { get; }

        public ElemMatchFilter(Expression<Func<TModel, IEnumerable<TItem>>> field, IFilter<TItem> filter) {
            this.Field = field;
            this.Filter = filter;
        }

        public TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
