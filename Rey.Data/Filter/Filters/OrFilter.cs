﻿using System.Collections.Generic;

namespace Rey.Data {
    public class OrFilter<TModel> : CompositionFilter<TModel>, IOrFilter<TModel> {
        public OrFilter(IEnumerable<IFilter<TModel>> filters)
            : base(filters) {
        }

        public override TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
