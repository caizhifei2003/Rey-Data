﻿using System.Collections.Generic;

namespace Rey.Data {
    public class AndFilter<TModel> : CompositionFilter<TModel>, IAndFilter<TModel> {
        public AndFilter(IEnumerable<IFilter<TModel>> filters)
            : base(filters) {
        }

        public override TResult Convert<TResult>(IFilterConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
