﻿using System;

namespace Rey.Data {
    [AttributeUsage(AttributeTargets.Class)]
    public class RepositoryAttribute : Attribute {
        public string Name { get; }

        public RepositoryAttribute(string name) {
            this.Name = name;
        }
    }
}
