﻿namespace Rey.Data {
    public interface IModel<TKey> {
        TKey Id { get; }
    }
}
