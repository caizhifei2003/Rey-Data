﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public static class Sort<TModel> {
        public static ISort<TModel> Build(Action<ISortBuilder<TModel>> build) {
            var builder = new DefaultSortBuilder<TModel>();
            build.Invoke(builder);
            return builder.Build();
        }

        public static ISort<TModel> Ascending(Expression<Func<TModel, object>> field) {
            return Build(builder => builder.Ascending(field));
        }

        public static ISort<TModel> Descending(Expression<Func<TModel, object>> field) {
            return Build(builder => builder.Descending(field));
        }

        public static ISort<TModel> Direction(IQuerySortDirection direction, Expression<Func<TModel, object>> field) {
            if (direction.Equals(QuerySortDirection.Asc))
                return Ascending(field);

            if (direction.Equals(QuerySortDirection.Desc))
                return Descending(field);

            throw new InvalidOperationException($"invalid direction {direction}");
        }
    }
}
