﻿using System.Collections.Generic;
using System.Linq;

namespace Rey.Data {
    public class DefaultSortBuilder<TModel> : SortBuilder<TModel> {
        protected override ISort<TModel> Build(IEnumerable<ISort<TModel>> sorts) {
            var count = sorts.Count();
            if (count == 0)
                return null;

            if (count == 1)
                return sorts.First();

            return new CombineSort<TModel>(sorts);
        }
    }
}
