﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Data {
    public abstract class SortBuilder<TModel> : ISortBuilder<TModel> {
        private List<ISort<TModel>> Sorts { get; } = new List<ISort<TModel>>();

        protected virtual ISortBuilder<TModel> OnSort(ISort<TModel> sort) {
            this.Sorts.Add(sort);
            return this;
        }

        public ISort<TModel> Build() {
            return this.Build(this.Sorts);
        }

        protected abstract ISort<TModel> Build(IEnumerable<ISort<TModel>> sorts);

        public ISortBuilder<TModel> Ascending(Expression<Func<TModel, object>> field) {
            return this.OnSort(new AscendingSort<TModel>(field));
        }

        public ISortBuilder<TModel> Descending(Expression<Func<TModel, object>> field) {
            return this.OnSort(new DescendingSort<TModel>(field));
        }

        public ISortBuilder<TModel> Direction(IQuerySortDirection direction, Expression<Func<TModel, object>> field) {
            return this.OnSort(Sort<TModel>.Direction(direction, field));
        }
    }
}
