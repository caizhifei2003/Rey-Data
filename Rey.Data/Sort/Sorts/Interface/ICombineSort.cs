﻿using System.Collections.Generic;

namespace Rey.Data {
    public interface ICombineSort<TModel> : ISort<TModel> {
        IEnumerable<ISort<TModel>> Sorts { get; }
    }
}
