﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IDescendingSort<TModel> : ISort<TModel> {
        Expression<Func<TModel, object>> Field { get; }
    }
}
