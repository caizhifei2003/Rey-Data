﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface IAscendingSort<TModel> : ISort<TModel> {
        Expression<Func<TModel, object>> Field { get; }
    }
}
