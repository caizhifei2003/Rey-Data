﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class DescendingSort<TModel> : IDescendingSort<TModel> {
        public Expression<Func<TModel, object>> Field { get; }

        public DescendingSort(Expression<Func<TModel, object>> field) {
            this.Field = field;
        }

        public TResult Convert<TResult>(ISortConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
