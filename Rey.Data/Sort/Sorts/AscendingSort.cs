﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public class AscendingSort<TModel> : IAscendingSort<TModel> {
        public Expression<Func<TModel, object>> Field { get; }

        public AscendingSort(Expression<Func<TModel, object>> field) {
            this.Field = field;
        }

        public TResult Convert<TResult>(ISortConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
