﻿using System.Collections.Generic;

namespace Rey.Data {
    public class CombineSort<TModel> : ICombineSort<TModel> {
        public IEnumerable<ISort<TModel>> Sorts { get; }

        public CombineSort(IEnumerable<ISort<TModel>> sorts) {
            this.Sorts = sorts;
        }

        public TResult Convert<TResult>(ISortConverter<TModel, TResult> converter) {
            return converter.Convert(this);
        }
    }
}
