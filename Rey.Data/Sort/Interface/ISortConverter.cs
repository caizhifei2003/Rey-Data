﻿namespace Rey.Data {
    public interface ISortConverter<TModel, TResult> {
        TResult Convert(IAscendingSort<TModel> sort);
        TResult Convert(IDescendingSort<TModel> sort);
        TResult Convert(ICombineSort<TModel> sort);
    }
}
