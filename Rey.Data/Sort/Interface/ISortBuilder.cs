﻿using System;
using System.Linq.Expressions;

namespace Rey.Data {
    public interface ISortBuilder<TModel> {
        ISort<TModel> Build();

        ISortBuilder<TModel> Ascending(Expression<Func<TModel, object>> field);
        ISortBuilder<TModel> Descending(Expression<Func<TModel, object>> field);
        ISortBuilder<TModel> Direction(IQuerySortDirection direction, Expression<Func<TModel, object>> field);
    }
}
