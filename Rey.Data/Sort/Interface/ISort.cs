﻿namespace Rey.Data {
    public interface ISort<TModel> {
        TResult Convert<TResult>(ISortConverter<TModel, TResult> converter);
    }
}
