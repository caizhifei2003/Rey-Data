﻿using MongoDB.Driver;
using System.Linq;

namespace Rey.Data {
    public class ReyMongoFilterConverter<TModel> : IFilterConverter<TModel, FilterDefinition<TModel>> {
        public FilterDefinition<TModel> Convert(INotFilter<TModel> filter) {
            return Builders<TModel>.Filter.Not(filter.Filter.Convert(this));
        }

        public FilterDefinition<TModel> Convert(IAndFilter<TModel> filter) {
            return Builders<TModel>.Filter.And(filter.Filters.Select(x => x.ConvertMongo()));
        }

        public FilterDefinition<TModel> Convert(IOrFilter<TModel> filter) {
            return Builders<TModel>.Filter.Or(filter.Filters.Select(x => x.ConvertMongo()));
        }

        public FilterDefinition<TModel> Convert<TField>(IEqFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Eq(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TField>(INeFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Ne(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TField>(IGtFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Gt(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TField>(IGteFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Gte(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TField>(ILtFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Lt(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TField>(ILteFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Lte(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyEqFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyEq(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyNeFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyNe(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyGtFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyGt(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyGteFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyGte(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyLtFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyLt(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyLteFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyLte(filter.Field, filter.Value);
        }

        public FilterDefinition<TModel> Convert(ISizeFilter<TModel> filter) {
            return Builders<TModel>.Filter.Size(filter.Field, filter.Size);
        }

        public FilterDefinition<TModel> Convert(ISizeGtFilter<TModel> filter) {
            return Builders<TModel>.Filter.SizeGt(filter.Field, filter.Size);
        }

        public FilterDefinition<TModel> Convert(ISizeGteFilter<TModel> filter) {
            return Builders<TModel>.Filter.SizeGte(filter.Field, filter.Size);
        }

        public FilterDefinition<TModel> Convert(ISizeLtFilter<TModel> filter) {
            return Builders<TModel>.Filter.SizeLt(filter.Field, filter.Size);
        }

        public FilterDefinition<TModel> Convert(ISizeLteFilter<TModel> filter) {
            return Builders<TModel>.Filter.SizeLte(filter.Field, filter.Size);
        }

        public FilterDefinition<TModel> Convert<TField>(IInFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.In(filter.Field, filter.Values);
        }

        public FilterDefinition<TModel> Convert<TField>(INinFilter<TModel, TField> filter) {
            return Builders<TModel>.Filter.Nin(filter.Field, filter.Values);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAllFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.All(filter.Field, filter.Values);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyInFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyIn(filter.Field, filter.Values);
        }

        public FilterDefinition<TModel> Convert<TItem>(IAnyNinFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.AnyNin(filter.Field, filter.Values);
        }

        public FilterDefinition<TModel> Convert<TItem>(IElemMatchFilter<TModel, TItem> filter) {
            return Builders<TModel>.Filter.ElemMatch(filter.Field, filter.Filter.ConvertMongo());
        }

        public FilterDefinition<TModel> Convert(IBitsAllClearFilter<TModel> filter) {
            return Builders<TModel>.Filter.BitsAllClear(filter.Field, filter.Bitmask);
        }

        public FilterDefinition<TModel> Convert(IBitsAllSetFilter<TModel> filter) {
            return Builders<TModel>.Filter.BitsAllSet(filter.Field, filter.Bitmask);
        }

        public FilterDefinition<TModel> Convert(IBitsAnyClearFilter<TModel> filter) {
            return Builders<TModel>.Filter.BitsAnyClear(filter.Field, filter.Bitmask);
        }

        public FilterDefinition<TModel> Convert(IBitsAnySetFilter<TModel> filter) {
            return Builders<TModel>.Filter.BitsAnySet(filter.Field, filter.Bitmask);
        }

        public FilterDefinition<TModel> Convert(IExistsFilter<TModel> filter) {
            return Builders<TModel>.Filter.Exists(filter.Field, filter.Exists);
        }

        public FilterDefinition<TModel> Convert(IModFilter<TModel> filter) {
            return Builders<TModel>.Filter.Mod(filter.Field, filter.Modulus, filter.Remainder);
        }

        public FilterDefinition<TModel> Convert(IRegexFilter<TModel> filter) {
            return Builders<TModel>.Filter.Regex(filter.Field, filter.Regex);
        }

        public FilterDefinition<TModel> Convert(IRegexAnyFilter<TModel> filter) {
            var regs = filter.Regex.ToList();
            if (regs.Count == 0)
                return Builders<TModel>.Filter.Empty;

            if (regs.Count == 1)
                return Builders<TModel>.Filter.Regex(filter.Field, regs.First());

            return Builders<TModel>.Filter.Or(regs.Select(reg =>
                Builders<TModel>.Filter.Regex(filter.Field, reg)
            ));
        }

        public FilterDefinition<TModel> Convert(IRegexAllFilter<TModel> filter) {
            var regs = filter.Regex.ToList();
            if (regs.Count == 0)
                return Builders<TModel>.Filter.Empty;

            if (regs.Count == 1)
                return Builders<TModel>.Filter.Regex(filter.Field, regs.First());

            return Builders<TModel>.Filter.And(regs.Select(reg =>
                Builders<TModel>.Filter.Regex(filter.Field, reg)
            ));
        }

        public FilterDefinition<TModel> Convert(ITextFilter<TModel> filter) {
            if (filter.Language != null)
                return Builders<TModel>.Filter.Text(filter.Search, filter.Language);

            return Builders<TModel>.Filter.Text(filter.Search);
        }

        public FilterDefinition<TModel> Convert(IWhereFilter<TModel> filter) {
            return Builders<TModel>.Filter.Where(filter.Expresson);
        }
    }
}
