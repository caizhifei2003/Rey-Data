﻿using MongoDB.Driver;

namespace Rey.Data {
    public static class ReyMongoFilterConverterExtensions {
        public static FilterDefinition<TModel> ConvertMongo<TModel>(this IFilter<TModel> filter) {
            return filter.Convert(new ReyMongoFilterConverter<TModel>());
        }
    }
}
