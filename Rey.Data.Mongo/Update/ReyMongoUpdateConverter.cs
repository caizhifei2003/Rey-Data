﻿using MongoDB.Driver;
using System.Linq;

namespace Rey.Data {
    public class ReyMongoUpdateConverter<TModel> : IUpdateConverter<TModel, UpdateDefinition<TModel>> {
        public UpdateDefinition<TModel> Convert<TItem>(IAddToSetUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.AddToSet(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TItem>(IAddToSetEachUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.AddToSetEach(update.Field, update.Values);
        }

        public UpdateDefinition<TModel> Convert<TField>(IBitwiseAndUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.BitwiseAnd(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TField>(IBitwiseOrUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.BitwiseOr(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TField>(IBitwiseXorUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.BitwiseXor(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert(ICombineUpdate<TModel> update) {
            return Builders<TModel>.Update.Combine(update.Updates.Select(x => x.ConvertMongo()));
        }

        public UpdateDefinition<TModel> Convert(ICurrentDateUpdate<TModel> update) {
            return Builders<TModel>.Update.CurrentDate(update.Field);
        }

        public UpdateDefinition<TModel> Convert<TField>(IIncUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.Inc(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TField>(IMaxUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.Max(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TField>(IMinUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.Min(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TField>(IMulUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.Mul(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert(IPopFirstUpdate<TModel> update) {
            return Builders<TModel>.Update.PopFirst(update.Field);
        }

        public UpdateDefinition<TModel> Convert(IPopLastUpdate<TModel> update) {
            return Builders<TModel>.Update.PopLast(update.Field);
        }

        public UpdateDefinition<TModel> Convert<TItem>(IPullUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.Pull(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TItem>(IPullAllUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.PullAll(update.Field, update.Values);
        }

        public UpdateDefinition<TModel> Convert<TItem>(IPullFilterUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.PullFilter(update.Field, update.Filter.ConvertMongo());
        }

        public UpdateDefinition<TModel> Convert<TItem>(IPushUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.Push(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TItem>(IPushEachUpdate<TModel, TItem> update) {
            return Builders<TModel>.Update.PushEach(update.Field, update.Values);
        }

        public UpdateDefinition<TModel> Convert(IRenameUpdate<TModel> update) {
            return Builders<TModel>.Update.Rename(update.Field, update.NewName);
        }

        public UpdateDefinition<TModel> Convert<TField>(ISetUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.Set(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert<TField>(ISetOnInsertUpdate<TModel, TField> update) {
            return Builders<TModel>.Update.SetOnInsert(update.Field, update.Value);
        }

        public UpdateDefinition<TModel> Convert(IUnsetUpdate<TModel> update) {
            return Builders<TModel>.Update.Unset(update.Field);
        }
    }
}
