﻿using MongoDB.Driver;

namespace Rey.Data {
    public static class ReyMongoUpdateConverterExtensions {
        public static UpdateDefinition<TModel> ConvertMongo<TModel>(this IUpdate<TModel> update) {
            return update.Convert(new ReyMongoUpdateConverter<TModel>());
        }
    }
}
