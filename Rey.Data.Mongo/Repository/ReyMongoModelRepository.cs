﻿namespace Rey.Data {
    public class ReyMongoModelRepository<TModel, TKey> : ReyMongoRepository<TModel>, IModelRepository<TModel, TKey>
        where TModel : class, IModel<TKey> {
        public ReyMongoModelRepository(IDatabase database, string name = null)
            : base(database, name) {
        }

        public TModel UpdateOne(TKey id, IUpdate<TModel> update) {
            return this.UpdateOne(Filter<TModel>.Eq(x => x.Id, id), update);
        }

        public TModel ReplaceOne(TKey id, TModel model) {
            return this.ReplaceOne(Filter<TModel>.Eq(x => x.Id, id), model);
        }

        public TModel DeleteOne(TKey id) {
            return this.DeleteOne(Filter<TModel>.Eq(x => x.Id, id));
        }

        public TModel FindOne(TKey id) {
            return this.FindOne(Filter<TModel>.Eq(x => x.Id, id));
        }
    }
}
