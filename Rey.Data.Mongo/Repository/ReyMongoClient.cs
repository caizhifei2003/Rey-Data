﻿using MongoDB.Driver;

namespace Rey.Data {
    public class ReyMongoClient : IClient {
        internal IReyMongoClientOptions Options { get; }
        internal IMongoClient Client { get; }

        public ReyMongoClient(IReyMongoClientOptions options) {
            this.Options = options;
            this.Client = new MongoClient(options.ToMongoClientSettings());
        }

        public IDatabase GetDatabase(string name = null) {
            return new ReyMongoDatabase(this, name);
        }
    }
}
