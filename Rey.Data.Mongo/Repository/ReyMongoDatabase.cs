﻿using MongoDB.Driver;
using System;

namespace Rey.Data {
    public class ReyMongoDatabase : IDatabase {
        internal IMongoDatabase Database { get; }

        public ReyMongoDatabase(IClient client, string name = null) {
            var reyClient = client as ReyMongoClient;
            name = name ?? reyClient.Options.Database;

            if (name == null)
                throw new InvalidOperationException("database name is null");

            this.Database = reyClient.Client.GetDatabase(name);
        }

        public IRepository<TModel> GetRepository<TModel>(string name = null) {
            return new ReyMongoRepository<TModel>(this, name);
        }

        public IModelRepository<TModel, TKey> GetModelRepository<TModel, TKey>(string name = null)
            where TModel : class, IModel<TKey> {
            return new ReyMongoModelRepository<TModel, TKey>(this, name);
        }
    }
}
