﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Rey.Data {
    public class ReyMongoRepository<TModel> : IRepository<TModel> {
        private IMongoCollection<TModel> Collection { get; }

        public ReyMongoRepository(IDatabase database, string name = null) {
            var reyDB = database as ReyMongoDatabase;
            name = name ?? typeof(TModel).GetTypeInfo().GetCustomAttribute<RepositoryAttribute>()?.Name;

            if (name == null)
                throw new InvalidOperationException("repository name is null");

            this.Collection = reyDB.Database.GetCollection<TModel>(name);
        }

        public TModel InsertOne(TModel model) {
            this.Collection.InsertOne(model);
            return model;
        }

        public TModel UpdateOne(IFilter<TModel> filter, IUpdate<TModel> update) {
            return this.Collection.FindOneAndUpdate(
                filter.ConvertMongo(),
                update.ConvertMongo()
            );
        }

        public TModel ReplaceOne(IFilter<TModel> filter, TModel model) {
            return this.Collection.FindOneAndReplace(filter.ConvertMongo(), model);
        }

        public TModel DeleteOne(IFilter<TModel> filter) {
            return this.Collection.FindOneAndDelete(filter.ConvertMongo());
        }

        public TModel FindOne(IFilter<TModel> filter) {
            return this.Collection.Find(filter.ConvertMongo()).FirstOrDefault();
        }

        public long InsertMany(IEnumerable<TModel> models) {
            this.Collection.InsertMany(models);
            return models.Count();
        }

        public long UpdateMany(IFilter<TModel> filter, IUpdate<TModel> update) {
            return this.Collection.UpdateMany(
                filter.ConvertMongo(),
                update.ConvertMongo()
            ).ModifiedCount;
        }

        public long DeleteMany(IFilter<TModel> filter) {
            return this.Collection.DeleteMany(filter.ConvertMongo()).DeletedCount;
        }

        public IEnumerable<TModel> FindMany(IFilter<TModel> filter) {
            return this.Collection.Find(filter.ConvertMongo()).ToEnumerable();
        }

        public long Count(IFilter<TModel> filter) {
            return this.Collection.Find(filter.ConvertMongo()).Count();
        }

        public QueryResult<TModel> Query(IFilter<TModel> filter, ISort<TModel> sort = null, IQueryPage page = null) {
            var query = this.Collection.Find(filter.ConvertMongo());
            var total = query.Count();
            var skip = 0L;

            if (page != null && total > 0) {
                if (page.Skip.HasValue) {
                    query = query.Skip((int?)page.Skip);
                }

                if (page.Take.HasValue) {
                    query = query.Limit((int?)page.Take);
                }
            }

            if (sort != null && total > 0) {
                query = query.Sort(sort.ConvertMongo());
            }

            return new QueryResult<TModel>(query.ToList(), total, skip);
        }
    }
}
