﻿using MongoDB.Driver;

namespace Rey.Data {
    public interface IReyMongoClientOptions {
        string Host { get; }
        int Port { get; }
        string Database { get; }
        string AuthDB { get; }
        string User { get; }
        string Password { get; }

        MongoClientSettings ToMongoClientSettings();
    }
}
