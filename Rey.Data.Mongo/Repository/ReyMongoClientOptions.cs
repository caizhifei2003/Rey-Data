﻿using MongoDB.Driver;

namespace Rey.Data {
    public class ReyMongoClientOptions : IReyMongoClientOptions {
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 27017;
        public string Database { get; set; }
        public string AuthDB { get; set; } = "admin";
        public string User { get; set; }
        public string Password { get; set; }

        public MongoClientSettings ToMongoClientSettings() {
            var settings = new MongoClientSettings();
            settings.Server = new MongoServerAddress(this.Host, this.Port);
            if (this.User != null) {
                settings.Credential = MongoCredential.CreateCredential(this.AuthDB, this.User, this.Password);
            }
            return settings;
        }
    }
}
