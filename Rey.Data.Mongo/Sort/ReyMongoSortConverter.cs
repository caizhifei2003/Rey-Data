﻿using MongoDB.Driver;
using System.Linq;

namespace Rey.Data {
    public class ReyMongoSortConverter<TModel> : ISortConverter<TModel, SortDefinition<TModel>> {
        public SortDefinition<TModel> Convert(IAscendingSort<TModel> sort) {
            return Builders<TModel>.Sort.Ascending(sort.Field);
        }

        public SortDefinition<TModel> Convert(IDescendingSort<TModel> sort) {
            return Builders<TModel>.Sort.Descending(sort.Field);
        }

        public SortDefinition<TModel> Convert(ICombineSort<TModel> sort) {
            return Builders<TModel>.Sort.Combine(sort.Sorts.Select(x => x.ConvertMongo()));
        }
    }
}
