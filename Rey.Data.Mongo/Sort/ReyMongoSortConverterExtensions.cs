﻿using MongoDB.Driver;

namespace Rey.Data {
    public static class ReyMongoSortConverterExtensions {
        public static SortDefinition<TModel> ConvertMongo<TModel>(this ISort<TModel> sort) {
            return sort.Convert(new ReyMongoSortConverter<TModel>());
        }
    }
}
