﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Rey.Data {
    public static class MongoDefinitionExtensions {
        public static BsonDocument ToBsonDocument<T>(this FilterDefinition<T> filter) {
            var serializerRegistry = BsonSerializer.SerializerRegistry;
            var documentSerializer = serializerRegistry.GetSerializer<T>();
            return filter.Render(documentSerializer, serializerRegistry);
        }

        public static BsonDocument ToBsonDocument<T>(this UpdateDefinition<T> update) {
            var serializerRegistry = BsonSerializer.SerializerRegistry;
            var documentSerializer = serializerRegistry.GetSerializer<T>();
            return update.Render(documentSerializer, serializerRegistry);
        }

        public static BsonDocument ToBsonDocument<T>(this SortDefinition<T> sort) {
            var serializerRegistry = BsonSerializer.SerializerRegistry;
            var documentSerializer = serializerRegistry.GetSerializer<T>();
            return sort.Render(documentSerializer, serializerRegistry);
        }

        public static string ToJsonString<T>(this FilterDefinition<T> filter) {
            return ToBsonDocument(filter).ToString();
        }

        public static string ToJsonString<T>(this UpdateDefinition<T> update) {
            return ToBsonDocument(update).ToString();
        }

        public static string ToJsonString<T>(this SortDefinition<T> sort) {
            return ToBsonDocument(sort).ToString();
        }
    }
}
