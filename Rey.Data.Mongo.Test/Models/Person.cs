﻿using MongoDB.Bson;
using System.Collections.Generic;

namespace Rey.Data.Test.Models {
    public class Person {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Height { get; set; }
        public List<ObjectId> Children { get; set; }
        public List<ObjectId> Parents { get; set; }
        public List<double> BWH { get; set; }

        public List<PersonFavorite> Favorites { get; set; }
    }

    public class PersonFavorite {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public List<Person> Users { get; set; }
    }
}
