﻿using MongoDB.Driver;
using Rey.Data.Test.Models;
using System.Linq;
using Xunit;

namespace Rey.Data.Mongo {
    public class MyTest {
        [Fact]
        public void Test() {
            var options = new ReyMongoClientOptions() {
                User = "admin",
                Password = "admin123~"
            };
            var client = new ReyMongoClient(options);
            var db = client.GetDatabase("test");

            var repo = db.GetRepository<Person>("person");
            for (var i = 0; i < 100; ++i) {
                var model = new Person {
                    Name = $"name {i}"
                };
                repo.InsertOne(model);
            }

            var qr = repo.QueryAll();

            var jsonUpdate = Update<Person>.Build(update => update
                .Set(x => x.Name, "updated")
            ).ConvertMongo().ToJsonString();

            var jsonSort = Sort<Person>.Build(sort => sort
                .Ascending(x => x.Name)
                .Descending(x => x.Age)
            ).ConvertMongo().ToJsonString();

            var models = repo.FindAll().ToList();
            var count = repo.UpdateAll(update => update.Set(x => x.Name, "updated"));
            models = repo.FindAll().ToList();

            count = repo.DeleteAll();
        }
    }
}
