﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Rey.Data.Test.Expectation {
    public static class Expector {
        public static readonly string CONFIG_PATH = Path.Combine(Directory.GetCurrentDirectory(), "Expectation/config/filter.expect.txt");
        public static readonly string CONFIG_CONTENT = File.ReadAllText(CONFIG_PATH);

        public static string GetFilter(string name) {
            var group = Regex.Match(CONFIG_CONTENT, $"{Regex.Escape(name)}(\\s*)=(\\s*)(?<value>.*?)\\r?\\n?$", RegexOptions.Multiline)
                .Groups["value"];

            if (!group.Success)
                throw new InvalidOperationException($"cannot find expectation by [{name}]");

            return group.Value;
        }
    }
}
