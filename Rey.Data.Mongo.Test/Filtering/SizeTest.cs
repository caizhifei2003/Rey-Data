﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class SizeTest {
        [Fact(DisplayName = "Filter.Size")]
        public void TestSize() {
            var json = Filter<Person>.Size(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Size");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Size.Not")]
        public void TestSizeNot() {
            var json = Filter<Person>.Not(not => not
                .Size(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Size.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Size.And")]
        public void TestSizeAnd() {
            var json = Filter<Person>.And(and => and
                .Size(x => x.Height, 0xff)
                .Size(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Size.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Size.Or")]
        public void TestSizeOr() {
            var json = Filter<Person>.Or(or => or
                .Size(x => x.Height, 0xff)
                .Size(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Size.Or");
            Assert.Equal(expected, json);
        }
    }
}
