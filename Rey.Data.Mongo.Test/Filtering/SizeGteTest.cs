﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class SizeGteTest {
        [Fact(DisplayName = "Filter.SizeGte")]
        public void TestSizeGte() {
            var json = Filter<Person>.SizeGte(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGte.Not")]
        public void TestSizeGteNot() {
            var json = Filter<Person>.Not(not => not
                .SizeGte(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGte.And")]
        public void TestSizeGteAnd() {
            var json = Filter<Person>.And(and => and
                .SizeGte(x => x.Height, 0xff)
                .SizeGte(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGte.Or")]
        public void TestSizeGteOr() {
            var json = Filter<Person>.Or(or => or
                .SizeGte(x => x.Height, 0xff)
                .SizeGte(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGte.Or");
            Assert.Equal(expected, json);
        }
    }
}
