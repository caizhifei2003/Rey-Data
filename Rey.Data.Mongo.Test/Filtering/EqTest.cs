﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class EqTest {
        [Fact(DisplayName = "Filter.Eq")]
        public void TestEq() {
            var json = Filter<Person>.Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Eq");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Eq.Not")]
        public void TestEqNot() {
            var json = Filter<Person>.Not(not => not
                .Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Eq.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Eq.And")]
        public void TestEqAnd() {
            var json = Filter<Person>.And(and => and
                .Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Eq(x => x.Name, "kevin")
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Eq.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Eq.Or")]
        public void TestEqOr() {
            var json = Filter<Person>.Or(or => or
                .Eq(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Eq(x => x.Name, "kevin")
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Eq.Or");
            Assert.Equal(expected, json);
        }
    }
}
