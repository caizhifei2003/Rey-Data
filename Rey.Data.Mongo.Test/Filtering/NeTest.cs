﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class NeTest {
        [Fact(DisplayName = "Filter.Ne")]
        public void TestNe() {
            var json = Filter<Person>.Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Ne");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Ne.Not")]
        public void TestNeNot() {
            var json = Filter<Person>.Not(not => not
                .Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Ne.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Ne.And")]
        public void TestNeAnd() {
            var json = Filter<Person>.And(and => and
                .Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Ne(x => x.Name, "kevin")
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Ne.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Ne.Or")]
        public void TestNeOr() {
            var json = Filter<Person>.Or(or => or
                .Ne(x => x.Id, new ObjectId("5ae9252bb507d226e4650b95"))
                .Ne(x => x.Name, "kevin")
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Ne.Or");
            Assert.Equal(expected, json);
        }
    }
}
