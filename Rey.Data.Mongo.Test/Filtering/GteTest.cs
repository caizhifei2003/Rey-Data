﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class GteTest {
        [Fact(DisplayName = "Filter.Gte")]
        public void TestGte() {
            var json = Filter<Person>.Gte(x => x.Age, 18)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gte.Not")]
        public void TestGteNot() {
            var json = Filter<Person>.Not(not => not
                .Gte(x => x.Age, 18)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gte.And")]
        public void TestGteAnd() {
            var json = Filter<Person>.And(and => and
                .Gte(x => x.Age, 18)
                .Gte(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gte.Or")]
        public void TestGteOr() {
            var json = Filter<Person>.Or(or => or
                .Gte(x => x.Age, 18)
                .Gte(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gte.Or");
            Assert.Equal(expected, json);
        }
    }
}
