﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using System.Linq;
using Xunit;

namespace Rey.Data {
    public class ElemMatchTest {
        [Fact(DisplayName = "Filter.ElemMatch")]
        public void TestElemMatch() {
            var json = Filter<Person>
                .ElemMatch(x => x.Favorites, elem => elem.Eq(item => item.Name, "football"))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("ElemMatch");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.ElemMatch.Not")]
        public void TestElemMatchNot() {
            var json = Filter<Person>
                .ElemMatch(x => x.Favorites, elem => elem.Not(not => not
                    .Eq(item => item.Name, "football")
                ))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("ElemMatch.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.ElemMatch.And")]
        public void TestElemMatchAnd() {
            var json = Filter<Person>
                .ElemMatch(x => x.Favorites, elem => elem.And(and => and
                    .Eq(item => item.Name, "football")
                    .Eq(item => item.Name, "basketball")
                ))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("ElemMatch.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.ElemMatch.Or")]
        public void TestElemMatchOr() {
            var json = Filter<Person>
                .ElemMatch(x => x.Favorites, elem => elem.Or(or => or
                    .Eq(item => item.Name, "football")
                    .Eq(item => item.Name, "basketball")
                ))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("ElemMatch.Or");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.ElemMatch.Complex")]
        public void TestElemMatchComplex() {
            var children = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filter<Person>
                .ElemMatch(x => x.Favorites, elem => elem.Or(or => or
                    .Eq(item => item.Name, "football")
                    .ElemMatch(item => item.Users, elem2 => elem2.AnyIn(item2 => item2.Children, children))
                ))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("ElemMatch.Complex");
            Assert.Equal(expected, json);
        }
    }
}
