﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class BitsAnySetTest {
        [Fact(DisplayName = "Filter.BitsAnySet")]
        public void TestBitsAnySet() {
            var json = Filter<Person>.BitsAnySet(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnySet.Not")]
        public void TestBitsAnySetNot() {
            var json = Filter<Person>.Not(not => not
                .BitsAnySet(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnySet.And")]
        public void TestBitsAnySetAnd() {
            var json = Filter<Person>.And(and => and
                .BitsAnySet(x => x.Height, 0xff)
                .BitsAnySet(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnySet.Or")]
        public void TestBitsAnySetOr() {
            var json = Filter<Person>.Or(or => or
                .BitsAnySet(x => x.Height, 0xff)
                .BitsAnySet(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnySet.Or");
            Assert.Equal(expected, json);
        }
    }
}
