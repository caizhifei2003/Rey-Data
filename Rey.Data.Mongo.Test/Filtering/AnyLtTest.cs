﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class AnyLtTest {
        [Fact(DisplayName = "Filter.AnyLt")]
        public void TestAnyLt() {
            var json = Filter<Person>.AnyLt(x => x.BWH, 91.38)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLt.Not")]
        public void TestAnyLtNot() {
            var json = Filter<Person>.Not(not => not
                .AnyLt(x => x.BWH, 91.38)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLt.And")]
        public void TestAnyLtAnd() {
            var json = Filter<Person>.And(and => and
                .AnyLt(x => x.BWH, 91.38)
                .AnyLt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLt.Or")]
        public void TestAnyLtOr() {
            var json = Filter<Person>.Or(or => or
                .AnyLt(x => x.BWH, 91.38)
                .AnyLt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLt.Or");
            Assert.Equal(expected, json);
        }
    }
}
