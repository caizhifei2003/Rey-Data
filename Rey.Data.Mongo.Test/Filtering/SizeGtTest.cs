﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class SizeGtTest {
        [Fact(DisplayName = "Filter.SizeGt")]
        public void TestSizeGt() {
            var json = Filter<Person>.SizeGt(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGt.Not")]
        public void TestSizeGtNot() {
            var json = Filter<Person>.Not(not => not
                .SizeGt(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGt.And")]
        public void TestSizeGtAnd() {
            var json = Filter<Person>.And(and => and
                .SizeGt(x => x.Height, 0xff)
                .SizeGt(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeGt.Or")]
        public void TestSizeGtOr() {
            var json = Filter<Person>.Or(or => or
                .SizeGt(x => x.Height, 0xff)
                .SizeGt(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeGt.Or");
            Assert.Equal(expected, json);
        }
    }
}
