﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class ExistsTest {
        [Fact(DisplayName = "Filter.Exists")]
        public void TestExists() {
            var json = Filter<Person>.Exists(x => x.Name, true)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Exists");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Exists.Not")]
        public void TestExistsNot() {
            var json = Filter<Person>.Not(not => not
                .Exists(x => x.Name, true)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Exists.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Exists.And")]
        public void TestExistsAnd() {
            var json = Filter<Person>.And(and => and
                .Exists(x => x.Name, true)
                .Exists(x => x.Name, true)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Exists.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Exists.Or")]
        public void TestExistsOr() {
            var json = Filter<Person>.Or(or => or
                .Exists(x => x.Name, true)
                .Exists(x => x.Name, true)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Exists.Or");
            Assert.Equal(expected, json);
        }
    }
}
