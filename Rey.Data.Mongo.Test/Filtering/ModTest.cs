﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class ModTest {
        [Fact(DisplayName = "Filter.Mod")]
        public void TestMod() {
            var json = Filter<Person>.Mod(x => x.Height, 10, 3)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Mod");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Mod.Not")]
        public void TestModNot() {
            var json = Filter<Person>.Not(not => not
                .Mod(x => x.Height, 10, 3)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Mod.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Mod.And")]
        public void TestModAnd() {
            var json = Filter<Person>.And(and => and
                .Mod(x => x.Height, 10, 3)
                .Mod(x => x.Height, 10, 3)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Mod.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Mod.Or")]
        public void TestModOr() {
            var json = Filter<Person>.Or(or => or
                .Mod(x => x.Height, 10, 3)
                .Mod(x => x.Height, 10, 3)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Mod.Or");
            Assert.Equal(expected, json);
        }
    }
}
