﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class AnyLteTest {
        [Fact(DisplayName = "Filter.AnyLte")]
        public void TestAnyLte() {
            var json = Filter<Person>.AnyLte(x => x.BWH, 91.38)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLte.Not")]
        public void TestAnyLteNot() {
            var json = Filter<Person>.Not(not => not
                .AnyLte(x => x.BWH, 91.38)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLte.And")]
        public void TestAnyLteAnd() {
            var json = Filter<Person>.And(and => and
                .AnyLte(x => x.BWH, 91.38)
                .AnyLte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyLte.Or")]
        public void TestAnyLteOr() {
            var json = Filter<Person>.Or(or => or
                .AnyLte(x => x.BWH, 91.38)
                .AnyLte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyLte.Or");
            Assert.Equal(expected, json);
        }
    }
}
