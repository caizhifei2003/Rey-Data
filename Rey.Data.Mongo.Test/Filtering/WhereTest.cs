﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class WhereTest {
        [Fact(DisplayName = "Filter.Where")]
        public void TestWhere() {
            var json = Filter<Person>.Where(x => x.Height > 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Where");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Where.Not")]
        public void TestWhereNot() {
            var json = Filter<Person>.Not(not => not
                .Where(x => x.Height > 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Where.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Where.And")]
        public void TestWhereAnd() {
            var json = Filter<Person>.And(and => and
                .Where(x => x.Height > 100)
                .Where(x => x.Height < 200)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Where.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Where.Or")]
        public void TestWhereOr() {
            var json = Filter<Person>.Or(or => or
                .Where(x => x.Height > 100)
                .Where(x => x.Height < 200)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Where.Or");
            Assert.Equal(expected, json);
        }
    }
}
