﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class GtTest {
        [Fact(DisplayName = "Filter.Gt")]
        public void TestGt() {
            var json = Filter<Person>.Gt(x => x.Age, 18)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gt.Not")]
        public void TestGtNot() {
            var json = Filter<Person>.Not(not => not
                .Gt(x => x.Age, 18)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gt.And")]
        public void TestGtAnd() {
            var json = Filter<Person>.And(and => and
                .Gt(x => x.Age, 18)
                .Gt(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Gt.Or")]
        public void TestGtOr() {
            var json = Filter<Person>.Or(or => or
                .Gt(x => x.Age, 18)
                .Gt(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Gt.Or");
            Assert.Equal(expected, json);
        }
    }
}
