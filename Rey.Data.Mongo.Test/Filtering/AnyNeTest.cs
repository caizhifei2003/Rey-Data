﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class AnyNeTest {
        [Fact(DisplayName = "Filter.AnyNe")]
        public void TestAnyNe() {
            var json = Filter<Person>.AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyNe");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyNe.Not")]
        public void TestAnyNeNot() {
            var json = Filter<Person>.Not(not => not
                .AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyNe.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyNe.And")]
        public void TestAnyNeAnd() {
            var json = Filter<Person>.And(and => and
                .AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyNe(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyNe.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyNe.Or")]
        public void TestAnyNeOr() {
            var json = Filter<Person>.Or(or => or
                .AnyNe(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyNe(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyNe.Or");
            Assert.Equal(expected, json);
        }
    }
}
