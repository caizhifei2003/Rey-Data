﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class SizeLtTest {
        [Fact(DisplayName = "Filter.SizeLt")]
        public void TestSizeLt() {
            var json = Filter<Person>.SizeLt(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLt.Not")]
        public void TestSizeLtNot() {
            var json = Filter<Person>.Not(not => not
                .SizeLt(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLt.And")]
        public void TestSizeLtAnd() {
            var json = Filter<Person>.And(and => and
                .SizeLt(x => x.Height, 0xff)
                .SizeLt(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLt.Or")]
        public void TestSizeLtOr() {
            var json = Filter<Person>.Or(or => or
                .SizeLt(x => x.Height, 0xff)
                .SizeLt(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLt.Or");
            Assert.Equal(expected, json);
        }
    }
}
