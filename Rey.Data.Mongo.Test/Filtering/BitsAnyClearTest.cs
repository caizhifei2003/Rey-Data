﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class BitsAnyClearTest {
        [Fact(DisplayName = "Filter.BitsAnyClear")]
        public void TestBitsAnyClear() {
            var json = Filter<Person>.BitsAnyClear(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnyClear.Not")]
        public void TestBitsAnyClearNot() {
            var json = Filter<Person>.Not(not => not
                .BitsAnyClear(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnyClear.And")]
        public void TestBitsAnyClearAnd() {
            var json = Filter<Person>.And(and => and
                .BitsAnyClear(x => x.Height, 0xff)
                .BitsAnyClear(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAnyClear.Or")]
        public void TestBitsAnyClearOr() {
            var json = Filter<Person>.Or(or => or
                .BitsAnyClear(x => x.Height, 0xff)
                .BitsAnyClear(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAnyClear.Or");
            Assert.Equal(expected, json);
        }
    }
}
