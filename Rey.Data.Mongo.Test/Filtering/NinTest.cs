﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class NinTest {
        [Fact(DisplayName = "Filter.Nin")]
        public void TestNin() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var json = Filter<Person>.Nin(x => x.Name, names)
               .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Nin");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Nin.Not")]
        public void TestNinNot() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var json = Filter<Person>.Not(not => not
                .Nin(x => x.Name, names)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Nin.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Nin.And")]
        public void TestNinAnd() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var ages = new int[] { 15, 18, 28 };
            var json = Filter<Person>.And(and => and
                .Nin(x => x.Name, names)
                .Nin(x => x.Age, ages)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Nin.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Nin.Or")]
        public void TestNinOr() {
            var names = new string[] { "kevin", "kenny", "anibei" };
            var ages = new int[] { 15, 18, 28 };
            var json = Filter<Person>.Or(or => or
                .Nin(x => x.Name, names)
                .Nin(x => x.Age, ages)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Nin.Or");
            Assert.Equal(expected, json);
        }
    }
}
