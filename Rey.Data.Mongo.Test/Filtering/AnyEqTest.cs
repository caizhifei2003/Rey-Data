﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class AnyEqTest {
        [Fact(DisplayName = "Filter.AnyEq")]
        public void TestAnyEq() {
            var json = Filter<Person>.AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyEq");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyEq.Not")]
        public void TestAnyEqNot() {
            var json = Filter<Person>.Not(not => not
                .AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyEq.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyEq.And")]
        public void TestAnyEqAnd() {
            var json = Filter<Person>.And(and => and
                .AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyEq(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyEq.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyEq.Or")]
        public void TestAnyEqOr() {
            var json = Filter<Person>.Or(or => or
                .AnyEq(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
                .AnyEq(x => x.Parents, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyEq.Or");
            Assert.Equal(expected, json);
        }
    }
}
