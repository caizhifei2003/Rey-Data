﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class LtTest {
        [Fact(DisplayName = "Filter.Lt")]
        public void TestLt() {
            var json = Filter<Person>.Lt(x => x.Age, 18)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lt.Not")]
        public void TestLtNot() {
            var json = Filter<Person>.Not(not => not
                .Lt(x => x.Age, 18)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lt.And")]
        public void TestLtAnd() {
            var json = Filter<Person>.And(and => and
                .Lt(x => x.Age, 18)
                .Lt(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lt.Or")]
        public void TestLtOr() {
            var json = Filter<Person>.Or(or => or
                .Lt(x => x.Age, 18)
                .Lt(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lt.Or");
            Assert.Equal(expected, json);
        }
    }
}
