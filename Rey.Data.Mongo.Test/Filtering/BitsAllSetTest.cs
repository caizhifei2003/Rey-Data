﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class BitsAllSetTest {
        [Fact(DisplayName = "Filter.BitsAllSet")]
        public void TestBitsAllSet() {
            var json = Filter<Person>.BitsAllSet(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllSet.Not")]
        public void TestBitsAllSetNot() {
            var json = Filter<Person>.Not(not => not
                .BitsAllSet(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllSet.And")]
        public void TestBitsAllSetAnd() {
            var json = Filter<Person>.And(and => and
                .BitsAllSet(x => x.Height, 0xff)
                .BitsAllSet(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllSet.Or")]
        public void TestBitsAllSetOr() {
            var json = Filter<Person>.Or(or => or
                .BitsAllSet(x => x.Height, 0xff)
                .BitsAllSet(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllSet.Or");
            Assert.Equal(expected, json);
        }
    }
}
