﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class AnyGtTest {
        [Fact(DisplayName = "Filter.AnyGt")]
        public void TestAnyGt() {
            var json = Filter<Person>.AnyGt(x => x.BWH, 91.38)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGt");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGt.Not")]
        public void TestAnyGtNot() {
            var json = Filter<Person>.Not(not => not
                .AnyGt(x => x.BWH, 91.38)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGt.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGt.And")]
        public void TestAnyGtAnd() {
            var json = Filter<Person>.And(and => and
                .AnyGt(x => x.BWH, 91.38)
                .AnyGt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGt.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGt.Or")]
        public void TestAnyGtOr() {
            var json = Filter<Person>.Or(or => or
                .AnyGt(x => x.BWH, 91.38)
                .AnyGt(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGt.Or");
            Assert.Equal(expected, json);
        }
    }
}
