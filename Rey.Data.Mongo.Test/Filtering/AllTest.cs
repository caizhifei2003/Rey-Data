﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using System.Linq;
using Xunit;

namespace Rey.Data {
    public class AllTest {
        [Fact(DisplayName = "Filter.All")]
        public void TestAll() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filter<Person>.All(x => x.Children, value)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("All");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.All.Not")]
        public void TestAllNot() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filter<Person>.Not(not => not
                .All(x => x.Children, value)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("All.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.All.And")]
        public void TestAllAnd() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filter<Person>.And(and => and
                .All(x => x.Children, value)
                .All(x => x.Parents, value)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("All.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.All.Or")]
        public void TestAllOr() {
            var value = new string[] {
                "5ae9252bb507d226e4650b95",
                "5ae9252bb507d226e4650b96"
            }.Select(x => new ObjectId(x));

            var json = Filter<Person>.Or(or => or
                .All(x => x.Children, value)
                .All(x => x.Parents, value)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("All.Or");
            Assert.Equal(expected, json);
        }
    }
}
