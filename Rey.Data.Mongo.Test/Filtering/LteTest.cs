﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class LteTest {
        [Fact(DisplayName = "Filter.Lte")]
        public void TestLte() {
            var json = Filter<Person>.Lte(x => x.Age, 18)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lte.Not")]
        public void TestLteNot() {
            var json = Filter<Person>.Not(not => not
                .Lte(x => x.Age, 18)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lte.And")]
        public void TestLteAnd() {
            var json = Filter<Person>.And(and => and
                .Lte(x => x.Age, 18)
                .Lte(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.Lte.Or")]
        public void TestLteOr() {
            var json = Filter<Person>.Or(or => or
                .Lte(x => x.Age, 18)
                .Lte(x => x.Height, 175)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("Lte.Or");
            Assert.Equal(expected, json);
        }
    }
}
