﻿using MongoDB.Bson;
using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class AnyGteTest {
        [Fact(DisplayName = "Filter.AnyGte")]
        public void TestAnyGte() {
            var json = Filter<Person>.AnyGte(x => x.BWH, 91.38)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGte.Not")]
        public void TestAnyGteNot() {
            var json = Filter<Person>.Not(not => not
                .AnyGte(x => x.BWH, 91.38)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGte.And")]
        public void TestAnyGteAnd() {
            var json = Filter<Person>.And(and => and
                .AnyGte(x => x.BWH, 91.38)
                .AnyGte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.AnyGte.Or")]
        public void TestAnyGteOr() {
            var json = Filter<Person>.Or(or => or
                .AnyGte(x => x.BWH, 91.38)
                .AnyGte(x => x.Children, new ObjectId("5ae9252bb507d226e4650b95"))
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("AnyGte.Or");
            Assert.Equal(expected, json);
        }
    }
}
