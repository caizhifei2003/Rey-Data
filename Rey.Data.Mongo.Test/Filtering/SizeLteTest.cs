﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class SizeLteTest {
        [Fact(DisplayName = "Filter.SizeLte")]
        public void TestSizeLte() {
            var json = Filter<Person>.SizeLte(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLte");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLte.Not")]
        public void TestSizeLteNot() {
            var json = Filter<Person>.Not(not => not
                .SizeLte(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLte.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLte.And")]
        public void TestSizeLteAnd() {
            var json = Filter<Person>.And(and => and
                .SizeLte(x => x.Height, 0xff)
                .SizeLte(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLte.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.SizeLte.Or")]
        public void TestSizeLteOr() {
            var json = Filter<Person>.Or(or => or
                .SizeLte(x => x.Height, 0xff)
                .SizeLte(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("SizeLte.Or");
            Assert.Equal(expected, json);
        }
    }
}
