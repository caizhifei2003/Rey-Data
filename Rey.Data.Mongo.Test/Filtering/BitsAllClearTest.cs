﻿using Rey.Data.Test.Expectation;
using Rey.Data.Test.Models;
using Xunit;

namespace Rey.Data {
    public class BitsAllClearTest {
        [Fact(DisplayName = "Filter.BitsAllClear")]
        public void TestBitsAllClear() {
            var json = Filter<Person>.BitsAllClear(x => x.Height, 0xff)
                .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllClear.Not")]
        public void TestBitsAllClearNot() {
            var json = Filter<Person>.Not(not => not
                .BitsAllClear(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear.Not");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllClear.And")]
        public void TestBitsAllClearAnd() {
            var json = Filter<Person>.And(and => and
                .BitsAllClear(x => x.Height, 0xff)
                .BitsAllClear(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear.And");
            Assert.Equal(expected, json);
        }

        [Fact(DisplayName = "Filter.BitsAllClear.Or")]
        public void TestBitsAllClearOr() {
            var json = Filter<Person>.Or(or => or
                .BitsAllClear(x => x.Height, 0xff)
                .BitsAllClear(x => x.Height, 0xff)
            )
            .ConvertMongo().ToJsonString();

            var expected = Expector.GetFilter("BitsAllClear.Or");
            Assert.Equal(expected, json);
        }
    }
}
